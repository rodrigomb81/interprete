import { Parser } from './parser/NewParser'
import { ModuloEjecutable } from './runtime_env/ModuloEjecutable'
import { FabricaModulosEjecutables } from './runtime_env/instrucciones/FabricaModulosEjecutables'

export class Compilador {

  private fabricaModulos: FabricaModulosEjecutables

  private parser: Parser

  private errores: any[] = []

  constructor(habilitarBreakpoints: boolean) {
    this.parser = new Parser()
    this.fabricaModulos = new FabricaModulosEjecutables(habilitarBreakpoints)
  }

  compilar(fuente: string): ModuloEjecutable[] {
    const ast = this.parser.analizar(fuente)

    if (ast.error == false) {
      // Falta consultar tabla de simbolos para ver si hay algun error
      return this.fabricaModulos.crearModulos(ast.valor)
    } else {
      this.errores = ast.valor
      return []
    }
  }

  getErrores() {
    return this.errores
  }
}