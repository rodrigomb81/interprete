import {
  valor_primitivo,
  Escalar,
  Vector,
  Referencia
} from "./TiposMemoria";
import { MemoriaPrograma } from './MemoriaPrograma'
import { Declaracion } from "../../parser/TiposParser";
import { EscalarConcreto } from "./EscalarConcreto";
import { VectorConcreto } from "./VectorConcreto";
import { esTipoPrimitivo } from '../../parser/tipado/esTipoPrimitivo';
import { TipoMatricial } from '../../parser/tipado/TipoMatricial';

export class MemoriaModulo {
  /**
   * Las variables de cada modulo indexadas
   * por el nombre del modulo.
   */
  private locales: Map<string, Referencia | Vector | Escalar> = new Map();

  private memoriaPrograma: MemoriaPrograma

  private contadorInstruccion: number = 0

  private bloquearModificacionContador = false

  constructor(locales: Declaracion[]) {
    this.inicializarVariables(locales)
  }

  desapilarModulo() {
    this.memoriaPrograma.desapilarModulo()
  }

  llamarModulo(nombre: string) {
    this.memoriaPrograma.llamarModulo(nombre)
  }

  getContadorInstruccion() {
    return this.contadorInstruccion
  }

  setContadorInstruccion(n: number) {
    this.contadorInstruccion = n
  }

  aumentarContadorInstruccion() {
    if (!this.bloquearModificacionContador) {
      this.contadorInstruccion++
    } else {
      this.bloquearModificacionContador = false
    }
  }

  decrementarContadorInstruccion() {
    if (!this.bloquearModificacionContador) {
      this.contadorInstruccion--
    } else {
      this.bloquearModificacionContador = false
    }
  }

  prevenirSiguienteModificacionContador() {
    this.bloquearModificacionContador = true
  }

  setMemoriaPrograma(memoriaPrograma: MemoriaPrograma) {
    this.memoriaPrograma = memoriaPrograma
  }

  /**
   * Inicializa las variables de este modulo.
   * 
   * Por ahora solo inicializa variables escalares y vectoriales.
   * 
   * @param declaraciones declaraciones de las variables a crear
   */
  private inicializarVariables(declaraciones: Declaracion[]) {
    for (let v of declaraciones) {
      if (esTipoPrimitivo(v.tipo_dato) == true) {
        this.locales.set(v.identificador, new EscalarConcreto())
      } else if (v.tipo_dato instanceof TipoMatricial) {
        this.locales.set(v.identificador, new VectorConcreto(v.tipo_dato.dimensiones))
      }
    }
  }

  desapilar() {
    return this.memoriaPrograma.desapilar()
  }

  desapilarVarios(cantidad: number): valor_primitivo[] {
    const desapilados: valor_primitivo[] = []
    for (let i = 0; i < cantidad; i++) {
      desapilados.unshift(this.memoriaPrograma.desapilar())
    }
    return desapilados
  }

  apilar(v: valor_primitivo) {
    this.memoriaPrograma.apilar(v)
  }

  apilarVarios(valores: valor_primitivo[]) {
    for (let v of valores) {
      this.memoriaPrograma.apilar(v)
    }
  }

  recuperarEscalar(nombre: string): Escalar {
    // const esLocal = nombre in this.locales
    const esLocal = this.locales.has(nombre)

    if (esLocal) {
      const variable = this.locales.get(nombre)
      if (variable.tipo == "referencia") {
        const resolucion = this.memoriaPrograma.resolver(variable);
        if (resolucion.tipo == "vectorial") {
          return resolucion.variable.obtenerCelda(resolucion.indices);
        } else {
          return resolucion.variable;
        }
      } else {
        return variable as Escalar;
      }
    } else {
      return this.memoriaPrograma.recuperarEscalarGlobal(nombre)
    }
  }

  recuperarVector(nombre: string): Vector {
    const esLocal = this.locales.has(nombre)

    if (esLocal) {
      const variable = this.locales.get(nombre)
      if (variable.tipo == "referencia") {
        const resolucion = this.memoriaPrograma.resolver(variable);
        return resolucion.variable as Vector;
      } else {
        return variable as Vector;
      }
    } else {
      return this.memoriaPrograma.recuperarVectorGlobal(nombre)
    }
  }

  recuperarVariable(nombre: string) {
    return this.locales.get(nombre)
  }

  escribirSalida(valor: valor_primitivo) {
    this.memoriaPrograma.solicitarEscritura(valor)
  }

  solicitarLectura() {
    this.memoriaPrograma.solicitarLectura()
  }

  detenerPrograma() {
    this.memoriaPrograma.detenerPrograma()
  }
}
