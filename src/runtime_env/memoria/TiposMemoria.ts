export type valor_primitivo = number | string | boolean

export type Referencia = {
  tipo: "referencia"
  /**
   * Nombre de la variable referenciada
   */
  nombreVariable: string

  /**
   * Indices utilizados al crear esta referencia
   */
  indices: number[]
}

export type ReferenciaResuelta = ReferenciaEscalar | ReferenciaVectorial

export type ReferenciaEscalar = {
  tipo: 'escalar'
  variable: Escalar
}

export type ReferenciaVectorial = {
  tipo: 'vectorial'
  variable: Vector
  indices: number[]
}

/**
 * Representa una porcion de memoria
 * que guarda muchos valores.
 */
export interface Vector {
  tipo: "vector"

  asignar(v: valor_primitivo, indices: number[])

  /**
   * Retorna el valor contenido en una celda especifica.
   * @param indices indices que especifican la celda deseada.
   */
  obtenerCelda(indices: number[]): Escalar

  /**
   * Retorna las celdas de este vector.
   */
  obtenerCeldas(): Escalar[]

  /**
   * Dice si este vector ha sido inicializado.
   * 
   * Un vector se considera inicializado si todas sus celdas
   * tienen un valor asignado.
   */
  inicializado(): boolean

  /**
   * Retorna las dimensiones de este vector.
   * 
   * Por "dimensiones" se entiende un arreglo de numeros
   * donde cada numero indica cuantos elementos puede
   * contener una dimension. Ejemplo: Un arreglo de 7
   * elementos tiene dimensiones = [7]. Una matriz de
   * 3x2 tiene dimensiones = [3, 2].
   */
  dimensiones(): number[]
}

/**
 * Representa una porcion de memoria
 * que guarda un solo valor.
 */
export interface Escalar {
  tipo: "escalar"

  asignar(v: valor_primitivo)

  // Deberia el tipo de retorno de esto
  // ser Optional<valor_primitivo> ???
  valor(): valor_primitivo

  inicializado(): boolean
}