export class IteradorArreglo<T> implements Iterator<T> {
  private arreglo: T[]
  private indice: number

  constructor(arreglo: T[]) {
    this.arreglo = arreglo
    this.indice = 0
  }

  next(): IteratorResult<T> {
    if (this.indice < this.arreglo.length) {
      const valor = this.arreglo[this.indice]
      this.indice++
      return { done: false, value: valor }
    } else {
      return { done: true, value: null }
    }
  }
}