import {
  valor_primitivo,
  Escalar,
  Vector,
  Referencia,
  ReferenciaResuelta
} from "./TiposMemoria";
import { ModuloEjecutable } from "../ModuloEjecutable";
import { MemoriaModulo } from "./MemoriaModulo";
import { Opcional } from '../../utility/Opcional';

export class MemoriaPrograma {
  /**
   * Pila de modulos en ejecucion.
   */
  private pila_modulos: ModuloEjecutable[]

  private modulos: Map<string, ModuloEjecutable>

  private pila: valor_primitivo[];

  private lecturaPendiente: boolean

  private ejecucionFinalizada: boolean

  private pila_salida: valor_primitivo[];

  constructor(modulos: ModuloEjecutable[]) {
    this.pila = [];
    this.pila_modulos = []
    this.pila_salida = []
    this.lecturaPendiente = false
    this.ejecucionFinalizada = false
    this.modulos = new Map()
    for (let modulo of modulos) {
      this.modulos.set(modulo.nombre, modulo)
      modulo.setMemoriaPrograma(this)
    }
    this.pila_modulos.push(this.modulos.get('principal'))
  }

  getModulo(nombre: string): Opcional<ModuloEjecutable> {
    if (this.modulos.has(nombre)) {
      return { existe: true, valor: this.modulos.get(nombre) }
    } else {
      return { existe: false, valor: null }
    }
  }

  programaFinalizado() {
    return this.ejecucionFinalizada
  }

  detenerPrograma() {
    this.ejecucionFinalizada = true
  }

  // Lectura y escritura
  solicitarEscritura(valor: valor_primitivo) {
    this.pila_salida.push(valor)
  }

  getProximaEscritura() {
    return this.pila_salida.shift()
  }

  enviarEntrada(v: valor_primitivo) {
    this.pila.push(v)
    this.lecturaPendiente = false
  }

  hayEscrituraPendiente() {
    return this.pila_salida.length > 0
  }

  hayLecturaPendiente() {
    return this.lecturaPendiente
  }

  apilarLectura(v: valor_primitivo) {
    this.pila.push(v)
  }

  solicitarLectura() {
    this.lecturaPendiente = true
  }

  // TODO LO RELACIONADO CON MODULOS

  getModuloActual(): ModuloEjecutable {
    const tope = this.pila_modulos.length - 1
    return this.pila_modulos[tope]
  }

  llamarModulo(nombre: string) {
    if (this.modulos.has(nombre)) {
      this.pila_modulos.push(this.modulos.get(nombre))
    } else {
      throw new Error(`No existe el modulo "${nombre}".`)
    }
  }

  desapilarModulo() {
    this.pila_modulos.pop()
  }

  // TODO LO RELACIONADO CON VARIABLES

  desapilar() {
    return this.pila.pop();
  }

  desapilarVarios(cantidad: number): valor_primitivo[] {
    const desapilados: valor_primitivo[] = []
    for (let i = 0; i < cantidad; i++) {
      desapilados.unshift(this.pila.pop())
    }
    return desapilados
  }

  apilar(v: valor_primitivo) {
    this.pila.push(v);
  }

  apilarVarios(valores: valor_primitivo[]) {
    for (let v of valores) {
      this.pila.push(v)
    }
  }

  recuperarEscalarGlobal(nombre: string): Escalar {
    return this.getGlobales().recuperarEscalar(nombre)
  }

  recuperarVectorGlobal(nombre: string): Vector {
    return this.getGlobales().recuperarVector(nombre)
  }

  private getGlobales(): MemoriaModulo {
    return this.pila_modulos[0].getMemoria()
  }

  /**
   * Convierte una referencia en una variable escalar o vectorial.
   *
   * Esa variable puede estar en cualquier MapaVariables entre el
   * "local" (el del tope de la pila) y el "global" (el del fondo)
   * (inclusive).
   * @param referencia referencia a una variable que pertenece a otro modulo.
   */
  resolver(referencia: Referencia): ReferenciaResuelta {
    return this._resolver(
      referencia,
      [...referencia.indices],
      this.pila_modulos.length - 2
    );
  }

  /**
   * Convierte una referencia en una variable escalar o vectorial.
   *
   * Esta es la funcion que hace todo el trabajo de resolver una
   * referencia. La otra (`resolver`) presenta una interfaz mas
   * facil de usar, nada mas.
   *
   * @param referencia referencia que hay que resolver.
   * @param indices indices que se han acumulado hasta ahora.
   * @param i indice que indica en cual de los ambitos hay que buscar
   */
  private _resolver(
    referencia: Referencia,
    indices: number[],
    i: number
  ): ReferenciaResuelta {
    if (i == 0) {
      const variable = this.pila_modulos[i].getMemoria().recuperarVariable(referencia.nombreVariable)

      // Si i == 0, entonces ya se recorrió toda la pila de
      // ambitos. No queda ningun otro lugar donde buscar
      // la variable referenciada. Y gracias al verificador
      // de tipado sabemos que si la variable no estaba en
      // ningun otro modulo, entonces está en este.

      if (indices.length > 0) {
        return { tipo: "vectorial", indices, variable: variable as Vector };
      } else {
        return { tipo: "escalar", variable: variable as Escalar };
      }
    } else {
      const variable = this.pila_modulos[i].getMemoria().recuperarVariable(referencia.nombreVariable)

      if (variable.tipo == "referencia") {
        return this._resolver(
          variable,
          [...indices, ...variable.indices],
          i - 1
        );
      } else {
        if (indices.length > 0) {
          return { tipo: "vectorial", indices, variable: variable as Vector };
        } else {
          return { tipo: "escalar", variable: variable as Escalar };
        }
      }
    }
  }
}
