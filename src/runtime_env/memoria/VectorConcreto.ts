import { valor_primitivo, Vector, Escalar } from "./TiposMemoria";
import { EscalarConcreto } from './EscalarConcreto'

export class VectorConcreto implements Vector {
  readonly tipo: "vector" = 'vector'
  private celdas: Escalar[]
  private _dimensiones: number[]

  constructor(d: number[]) {
    const longitud = d.reduce((a, b) => a * b)
    this.celdas = new Array<Escalar>(longitud)
    this._dimensiones = d.slice(0)
    for (let i = 0; i < longitud; i++) {
      this.celdas[i] = new EscalarConcreto()
    }
  }

  asignar(v: valor_primitivo, indices: number[]) {
    const celda = this.celdas[this.calcularIndice(indices)]
    celda.asignar(v)
  }

  obtenerCelda(indices: number[]): Escalar {
    // Resto 1 a cada indice porque en JS el primer
    // item de un arreglo tiene indice 0, y en mi
    // lenguaje el primer item tiene indice 1.
    
    const indice = this.calcularIndice(indices.map(i => i-1))

    return this.celdas[indice]
  }

  obtenerCeldas(): Escalar[] {
    return this.celdas
  }

  inicializado(): boolean {
    return this.celdas.every(c => c.inicializado())
  }

  dimensiones(): number[] {
    return this._dimensiones.slice(0)
  }

  private calcularIndice(indices: number[]) {
    let indice = 0
    let cantidadIndices = indices.length
    let i = 0

    while (i < cantidadIndices) {
        let term = 1
        let j = i + 1
        while (j < cantidadIndices) {
            term *= this._dimensiones[j]
            j++
        }
        term *= indices[i]
        indice += term
        i++
    }
    return indice
  }
}