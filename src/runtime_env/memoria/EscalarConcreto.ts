import { Escalar, valor_primitivo } from "./TiposMemoria";

export class EscalarConcreto implements Escalar {
  readonly tipo: "escalar" = 'escalar'
  private _valor: valor_primitivo
  private _inicializado: boolean = false

  constructor() {}

  asignar(v: valor_primitivo) {
    this._valor = v
    this._inicializado = true
  }

  valor(): valor_primitivo {
    return this._valor
  }

  inicializado(): boolean {
    return this._inicializado
  }
}