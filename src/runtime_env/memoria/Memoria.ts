import {
  valor_primitivo,
  Escalar,
  Vector,
  MapaVariables,
  Referencia,
  ReferenciaResuelta
} from "./TiposMemoria";
import { ModuloEjecutable } from "../ModuloEjecutable";

export class Memoria {
  // TODO LO RELACIONADO CON MODULOS
  /**
   * Pila de modulos en ejecucion.
   */
  private modulos: ModuloEjecutable[]

  // TODO LO RELACIONADO CON VARIABLES
  /**
   * Las variables de cada modulo indexadas
   * por el nombre del modulo.
   */
  private ambitos: MapaVariables[];
  private locales: MapaVariables;
  private globales: MapaVariables;

  private pila: valor_primitivo[];

  constructor() {
    this.ambitos = [];
    this.pila = [];
    this.locales = {};
    this.globales = {};
  }

  // TODO LO RELACIONADO CON MODULOS

  getModuloActual(): ModuloEjecutable {
    const tope = this.modulos.length - 1
    return this.modulos[tope]
  }

  /**
   * Pone al tope de la pila a un modulo.
   * @param modulo modulo a apilar
   */
  apilarModulo(modulo: ModuloEjecutable) {
    this.modulos.push(modulo)
  }

  /**
   * Desapila un modulo.
   */
  desapilarModulo() {
    this.pila.pop()
  }

  // TODO LO RELACIONADO CON VARIABLES

  desapilar() {
    return this.pila.pop();
  }

  desapilarVarios(cantidad: number): valor_primitivo[] {
    const desapilados: valor_primitivo[] = []
    for (let i = 0; i < cantidad; i++) {
      desapilados.unshift(this.pila.pop())
    }
    return desapilados
  }

  apilar(v: valor_primitivo) {
    this.pila.push(v);
  }

  apilarVarios(valores: valor_primitivo[]) {
    for (let v of valores) {
      this.pila.push(v)
    }
  }

  recuperarVariable(nombre: string): Escalar {
    // Hay que recuperar una variable. Puede estar en el
    // ambito local o en el ambito global.
    // Si es una referencia, en lugar de una variable
    // concreta, hay que resolverla.
    const variable =
      nombre in this.locales ? this.locales[nombre] : this.globales[nombre];

    if (variable.tipo == "referencia") {
      const resolucion = this.resolver(variable);
      if (resolucion.tipo == "vectorial") {
        return resolucion.variable.obtenerCelda(resolucion.indices);
      } else {
        return resolucion.variable;
      }
    } else {
      return variable as Escalar;
    }
  }

  recuperarVector(nombre: string): Vector {
    const variable =
      nombre in this.locales ? this.locales[nombre] : this.globales[nombre];

    if (variable.tipo == "referencia") {
      const resolucion = this.resolver(variable);
      return resolucion.variable as Vector;
    } else {
      return variable as Vector;
    }
  }

  /**
   * Convierte una referencia en una variable escalar o vectorial.
   *
   * Esa variable puede estar en cualquier MapaVariables entre el
   * "local" (el del tope de la pila) y el "global" (el del fondo)
   * (inclusive).
   * @param referencia referencia a una variable que pertenece a otro modulo.
   */
  resolver(referencia: Referencia): ReferenciaResuelta {
    return this._resolver(
      referencia,
      [...referencia.indices],
      this.ambitos.length - 2
    );
  }

  /**
   * Convierte una referencia en una variable escalar o vectorial.
   *
   * Esta es la funcion que hace todo el trabajo de resolver una
   * referencia. La otra (`resolver`) presenta una interfaz mas
   * facil de usar, nada mas.
   *
   * @param referencia referencia que hay que resolver.
   * @param indices indices que se han acumulado hasta ahora.
   * @param i indice que indica en cual de los ambitos hay que buscar
   */
  private _resolver(
    referencia: Referencia,
    indices: number[],
    i: number
  ): ReferenciaResuelta {
    if (i == 0) {
      const variable = this.ambitos[i][referencia.nombreVariable];

      // Si i == 0, entonces ya se recorrió toda la pila de
      // ambitos. No queda ningun otro lugar donde buscar
      // la variable referenciada. Y gracias al verificador
      // de tipado sabemos que si la variable no estaba en
      // ningun otro modulo, entonces está en este.

      if (indices.length > 0) {
        return { tipo: "vectorial", indices, variable: variable as Vector };
      } else {
        return { tipo: "escalar", variable: variable as Escalar };
      }
    } else {
      const variable = this.ambitos[i][referencia.nombreVariable];

      if (variable.tipo == "referencia") {
        return this._resolver(
          variable,
          [...indices, ...variable.indices],
          i - 1
        );
      } else {
        if (indices.length > 0) {
          return { tipo: "vectorial", indices, variable: variable as Vector };
        } else {
          return { tipo: "escalar", variable: variable as Escalar };
        }
      }
    }
  }
}
