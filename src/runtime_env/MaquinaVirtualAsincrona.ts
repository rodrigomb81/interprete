import { MaquinaVirtual } from './MaquinaVirtual'
import { ModuloEjecutable } from './ModuloEjecutable';
import { valor_primitivo } from './memoria/TiposMemoria';

/**
 * Envoltorio asincrono para la MaquinaVritual.
 * 
 * Está incompleto.
 */
export class MaquinaVirtualAsincrona extends MaquinaVirtual {

  callbackLectura: () => void
  callbackEscritura: (v: valor_primitivo) => void
  callbackFinalizar: () => void
  callbackResumir: () => void

  constructor() {
    super()
  }

  ejecutar() {
    setTimeout(() => {
      super.ejecutar()
      this.llamarCallbacks()
    }, 100)
  }

  resumir() {
    setTimeout(() => {
      setTimeout(this.callbackResumir, 100)
      super.resumir()
      this.llamarCallbacks()
    }, 100)
  }

  private llamarCallbacks() {
    while (this.hayLecturaPendiente() || this.hayEscrituraPendiente()) {
      if (this.hayEscrituraPendiente()) {
        setTimeout(() => { this.callbackEscritura(this.getProximaEscritura()) }, 100)
      } else {
        setTimeout(() => { this.callbackLectura() }, 100)
      }
    }

    if (this.finAlcanzado()) {
      this.callbackFinalizar()
    }
  }
}