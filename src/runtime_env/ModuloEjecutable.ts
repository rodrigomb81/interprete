import { Instruccion } from "./instrucciones/Instruccion";
import { MemoriaModulo } from "./memoria/MemoriaModulo";
import { Declaracion } from '../parser/TiposParser'
import { MemoriaPrograma } from "./memoria/MemoriaPrograma";

/**
 * Representa el contexto de ejecucion
 * de un modulo. Se instancia para
 * ejecutar las instrucciones de
 * un modulo.
 */
export class ModuloEjecutable {
  private instrucciones: Instruccion[]
  private memoria: MemoriaModulo
  readonly nombre: string

  constructor(nombre: string, instrucciones: Instruccion[], locales: Declaracion[]) {
    this.nombre = nombre
    this.instrucciones = instrucciones
    this.memoria = new MemoriaModulo(locales)
  }

  setMemoriaPrograma(mem: MemoriaPrograma) {
    this.memoria.setMemoriaPrograma(mem)
  }

  getMemoria(): MemoriaModulo {
    return this.memoria
  }

  breakpointAlcanzado() {
    return false
  }

  agregarBreakpoint(instruccion: number) {}

  quitarBreakpoint(instruccion: number) {}

  ejecutarInstruccion() {
    if (this.memoria.getContadorInstruccion() < this.instrucciones.length) {
      const instruccion = this.instrucciones[this.memoria.getContadorInstruccion()]
      instruccion.establecerMemoria(this.memoria)
      instruccion.ejecutar()
      this.memoria.aumentarContadorInstruccion()
    }
  }

  deshacerInstruccion() {
    if (this.memoria.getContadorInstruccion() >= 0) {
      const instruccion = this.instrucciones[this.memoria.getContadorInstruccion()]
      instruccion.establecerMemoria(this.memoria)
      instruccion.deshacer()
      this.memoria.decrementarContadorInstruccion()
    }
  }

  finAlcanzado() {
    return this.memoria.getContadorInstruccion() >= this.instrucciones.length
  }
}