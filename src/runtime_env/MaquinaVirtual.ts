import { Escalar, Vector, valor_primitivo } from "./memoria/TiposMemoria";
import { ModuloEjecutable } from "./ModuloEjecutable";
import { MemoriaPrograma } from "./memoria/MemoriaPrograma";
import { EstadoEvaluador, Estado, Evento } from './EstadoEvaluador'

export class MaquinaVirtual {
  private memoria: MemoriaPrograma;

  private estado: EstadoEvaluador = new EstadoEvaluador()

  setPrograma(modulos: ModuloEjecutable[]) {
    this.memoria = new MemoriaPrograma(modulos);
  }

  ejecutar() {
    let moduloActual = this.memoria.getModuloActual();

    while (this.estado.valor() == Estado.EJECUTANDO) {
      moduloActual.ejecutarInstruccion();
      moduloActual = this.memoria.getModuloActual();

      if (this.memoria.hayEscrituraPendiente() || this.memoria.hayLecturaPendiente()) {
        if (this.memoria.hayEscrituraPendiente()) {
          this.estado.transicion(Evento.ESCRIBIR)
        } else if (this.memoria.hayLecturaPendiente()) {
          this.estado.transicion(Evento.LEER)
        }
      } else if (this.memoria.programaFinalizado()) {
        this.estado.transicion(Evento.FINALIZAR)
      } else if (moduloActual.breakpointAlcanzado()) {
        this.estado.transicion(Evento.BREAKPOINT_ALCANZADO)
      }
    }
  }

  agregarBreakpointInstruccion(nombre: string, instruccion: number) {
    const modulo = this.memoria.getModulo(nombre)
    if (modulo.existe == true) {
      modulo.valor.agregarBreakpoint(instruccion)
    }
  }

  quitarBreakpointInstruccion(nombre: string, instruccion: number) {
    const modulo = this.memoria.getModulo(nombre)
    if (modulo.existe == true) {
      modulo.valor.quitarBreakpoint(instruccion)
    }
  }

  resumir() {
    this.estado.transicion(Evento.RESUMIR)
    this.ejecutar()
  }

  finAlcanzado() {
    return this.estado.valor() == Estado.FINALIZADO
  }

  ejecutarEnunciado() {}

  ejecutarInstruccion() {
    this.memoria.getModuloActual().ejecutarInstruccion();
  }

  deshacerInstruccion() {
    this.memoria.getModuloActual().deshacerInstruccion();
  }

  recuperarEscalarGlobal(nombre: string): Escalar {
    return this.memoria.recuperarEscalarGlobal(nombre);
  }

  recuperarVectorGlobal(nombre: string): Vector {
    return this.memoria.recuperarVectorGlobal(nombre);
  }

  // Lectura y escritura
  hayEscrituraPendiente() {
    return this.memoria.hayEscrituraPendiente()
  }

  hayLecturaPendiente() {
    return this.memoria.hayLecturaPendiente()
  }

  enviarEntrada(v: valor_primitivo) {
    return this.memoria.enviarEntrada(v)
  }

  getProximaEscritura() {
    return this.memoria.getProximaEscritura()
  }
}