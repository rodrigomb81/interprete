import { MaquinaVirtual } from '../MaquinaVirtual';
import { MemoriaModulo } from '../memoria/MemoriaModulo';

export abstract class Instruccion {

  protected memoria: MemoriaModulo
  protected evaluador: MaquinaVirtual

  constructor() {}
  
  establecerMemoria(mem: MemoriaModulo) {
    this.memoria = mem
  }

  establecerEvaluador(ev: MaquinaVirtual) {
    this.evaluador = ev
  }
  
  abstract ejecutar()

  abstract deshacer()

  getMemoria(): MemoriaModulo {
    return this.memoria
  }
}