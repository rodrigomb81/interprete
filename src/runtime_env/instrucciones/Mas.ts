import { OperacionBinaria } from "./OperacionBinaria";

export class Mas extends OperacionBinaria<number, number> {
  constructor() {
    super((a, b) => a + b)
  }
}