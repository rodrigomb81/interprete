import { Instruccion } from './Instruccion'

export class ApilarCeldaVector extends Instruccion {

  private nombreVector: string
  private cantidadIndices: number
  private indices: number[]

  constructor(nombreVector: string, indices: number) {
    super()
    this.nombreVector = nombreVector
    this.cantidadIndices = indices
  }

  ejecutar() {
    this.indices = this.getMemoria().desapilarVarios(this.cantidadIndices) as number[]
    const vector = this.getMemoria().recuperarVector(this.nombreVector)
    this.getMemoria().apilar(vector.obtenerCelda(this.indices).valor())
  }

  deshacer() {
    this.getMemoria().desapilar()
    this.getMemoria().apilarVarios(this.indices)
  }
}