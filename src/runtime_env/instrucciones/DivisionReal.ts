import { OperacionBinaria } from "./OperacionBinaria";

export class DivisionReal extends OperacionBinaria<number, number> {
  constructor() {
    super((a, b) => a / b)
  }
}