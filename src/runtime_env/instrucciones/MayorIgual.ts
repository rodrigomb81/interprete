import { OperacionBinaria } from "./OperacionBinaria";

export class MayorIgual extends OperacionBinaria<number, boolean> {
  constructor() {
    super((a, b) => a >= b)
  }
}