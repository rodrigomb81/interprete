import { OperacionBinaria } from "./OperacionBinaria";

export class And extends OperacionBinaria<boolean, boolean> {
  constructor() {
    super((a, b) => a && b)
  }
}