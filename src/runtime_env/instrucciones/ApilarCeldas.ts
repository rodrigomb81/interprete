import { Instruccion } from './Instruccion'
import { NumeroVectorial } from "../memoria/NumeroVectorial";

export class ApilarCeldas extends Instruccion {

  private nombreVector: string
  private dimensiones: number[]
  private indicesExistentes: number

  constructor(nombreVector: string, indicesExistentes: number, dimensiones: number[]) {
    super()
    this.nombreVector = nombreVector
    this.dimensiones = dimensiones
    this.indicesExistentes = indicesExistentes
  }

  ejecutar()  {
    const indiceParcial = this.getMemoria().desapilarVarios(this.indicesExistentes) as number[]
    const contador = new NumeroVectorial(this.dimensiones)
    contador.setValor(indiceParcial)
    const vector = this.getMemoria().recuperarVector(this.nombreVector)
    while (contador.menor(this.dimensiones) || contador.igual(this.dimensiones)) {
      const valor = vector.obtenerCelda(contador.comoArreglo()).valor()
      this.getMemoria().apilar(valor)
      contador.aumentar()
    }
  }

  deshacer() {}
}