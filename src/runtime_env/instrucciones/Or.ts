import { OperacionBinaria } from "./OperacionBinaria";

export class Or extends OperacionBinaria<boolean, boolean> {
  constructor() {
    super((a, b) => a || b)
  }
}