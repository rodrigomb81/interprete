import { OperacionBinaria } from "./OperacionBinaria";

export class Mod extends OperacionBinaria<number, number> {
  constructor() {
    super((a, b) => a % b)
  }
}