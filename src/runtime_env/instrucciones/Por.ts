import { OperacionBinaria } from "./OperacionBinaria";

export class Por extends OperacionBinaria<number, number> {
  constructor() {
    super((a, b) => a * b)
  }
}