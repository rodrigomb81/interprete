import { Instruccion } from "./Instruccion";
import { valor_primitivo } from '../memoria/TiposMemoria'

export class Apilar extends Instruccion {

  private v: valor_primitivo

  constructor(v: valor_primitivo) {
    super()
    this.v = v;
  }

  ejecutar() {
    super.getMemoria().apilar(this.v)
  }

  deshacer() {
    super.getMemoria().desapilar()
  }
}