import { OperacionBinaria } from "./OperacionBinaria";

export class Elevar extends OperacionBinaria<number, number> {
  constructor() {
    super((a, b) => a**b)
  }
}