import { OperacionBinaria } from "./OperacionBinaria";

export class Mayor extends OperacionBinaria<number, boolean> {
  constructor() {
    super((a, b) => a > b)
  }
}