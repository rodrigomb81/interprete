import { Instruccion } from './Instruccion'

/**
 * Instruccion que pone el valor de una variable escalar
 * al tope de la pila.
 */
export class ApilarVariable extends Instruccion {

  private nombreVariable: string

  constructor(nombreVariable: string) {
    super()
    this.nombreVariable = nombreVariable
  }

  ejecutar() {
    const variable = this.getMemoria().recuperarEscalar(this.nombreVariable)
    this.getMemoria().apilar(variable.valor())
  }

  deshacer() {
    this.getMemoria().desapilar()
  }
}