import { Instruccion } from './Instruccion'

export class Retornar extends Instruccion {
  ejecutar() {
    this.getMemoria().desapilarModulo()
  }

  deshacer() {}
}