import { Instruccion } from './Instruccion'

export class Not extends Instruccion {

  ejecutar() {
    const valor = this.getMemoria().desapilar() as boolean
    this.getMemoria().apilar(!valor)
  }

  deshacer() {
    const valor = this.getMemoria().desapilar() as boolean
    this.getMemoria().apilar(!valor)
  }
}