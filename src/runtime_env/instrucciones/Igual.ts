import { OperacionBinaria } from "./OperacionBinaria";

export class Igual extends OperacionBinaria<number | boolean | string, boolean> {
  constructor() {
    super((a, b) => a == b)
  }
}