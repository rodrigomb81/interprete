import { Instruccion } from './Instruccion'

export class SaltarSiVerdadero extends Instruccion {
  private destino: number
  private origen: number

  constructor(destino: number) {
    super()
    this.destino = destino
  }

  ejecutar() {
    this.origen = this.getMemoria().getContadorInstruccion()
    const expresion = this.getMemoria().desapilar() as boolean
    if (expresion == true) {
      this.getMemoria().setContadorInstruccion(this.destino)
      this.getMemoria().prevenirSiguienteModificacionContador()
    }
  }

  deshacer() {}
}