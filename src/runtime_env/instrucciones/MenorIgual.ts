import { OperacionBinaria } from "./OperacionBinaria";

export class MenorIgual extends OperacionBinaria<number, boolean> {
  constructor() {
    super((a, b) => a <= b)
  }
}