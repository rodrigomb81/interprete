import { Instruccion } from './Instruccion'

export class Saltar extends Instruccion {
  private destino: number
  private origen: number

  constructor(destino: number) {
    super()
    this.destino = destino
  }

  ejecutar() {
    this.origen = this.getMemoria().getContadorInstruccion()
    this.getMemoria().setContadorInstruccion(this.destino)
    this.getMemoria().prevenirSiguienteModificacionContador()
  }

  deshacer() {
    this.getMemoria().setContadorInstruccion(this.origen)
    this.getMemoria().prevenirSiguienteModificacionContador()
  }
}