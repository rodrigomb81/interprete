import { Instruccion } from './Instruccion'

export class Detener extends Instruccion {
  ejecutar() {
    this.getMemoria().detenerPrograma()
  }

  deshacer() {}
}