import { Instruccion } from "./Instruccion";
import { valor_primitivo } from '../memoria/TiposMemoria'

/**
 * Funcion que toma dos valores y retorna otro del
 * mismo tipo.
 * 
 * Generalizacion de del tipo de operaciones como
 * la suma, la resta, or, and. etc
 */
export type opbin<T, V> = (a: T, b: T) => V

export class OperacionBinaria<T extends valor_primitivo, V extends valor_primitivo> extends Instruccion {
  private a: T
  private b: T
  private operacion: opbin<T, V>

  constructor(op: opbin<T, V>) {
    super()
    this.operacion = op
  }

  ejecutar() {
    this.b = this.getMemoria().desapilar() as T
    this.a = this.getMemoria().desapilar() as T

    this.getMemoria().apilar(this.operacion(this.a, this.b))
  }

  deshacer() {
    this.getMemoria().desapilar()

    this.getMemoria().apilar(this.b)
    this.getMemoria().apilar(this.a)
  }
}