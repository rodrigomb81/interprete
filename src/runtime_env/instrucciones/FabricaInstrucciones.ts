import { Enunciado, Asignacion, Expresion as ArbolExpresion, Expresion, InvocacionVariable, Si, Mientras, Repetir, Para, EnunciadoLlamadaModulo, Declaracion, Retornar } from "../../parser/TiposParser";
import { Instruccion } from "./Instruccion";
import { Retornar as InstruccionRetornar } from './Retornar'
import { ApilarCeldas } from './ApilarCeldas'
import { LlamarModulo } from './LlamarModulo'
import { AsignarEscalar } from './AsignarEscalar'
import { AsignarCeldaVector } from './AsignarCeldaVector'
import { CopiarVector } from './CopiarVector'
import { SaltarSiFalso } from './SaltarSiFalso'
import { SaltarSiVerdadero } from './SaltarSiVerdadero'
import { Saltar } from './Saltar'
import { Apilar } from './Apilar'
import { And } from './And'
import { Or } from './Or'
import { Escribir } from './Escribir'
import { Diferente } from './Diferente'
import { Leer } from './Leer'
import { DivisionEntera } from './DivisionEntera'
import { DivisionReal } from './DivisionReal'
import { Elevar } from './Elevar'
import { Igual } from './Igual'
import { Mas } from './Mas'
import { Menos } from './Menos'
import { Mayor } from './Mayor'
import { MayorIgual } from './MayorIgual'
import { Menor } from './Menor'
import { MenorIgual } from './MenorIgual'
import { Mod } from './Mod'
import { ApilarVariable } from './ApilarVariable'
import { ApilarCeldaVector } from './ApilarCeldaVector'
import { Negativo } from './Negativo'
import { Not } from './Not'
import { Por } from './Por'
import { TipoMatricial } from "../../parser/tipado/TipoMatricial";
import { arbol_a_rpn, Operando, Operador, Invocacion } from "../../parser/ExpresionRPN";
import { TipoDato } from '../../parser/tipado/TipoDato';
import { esTipoPrimitivo } from './../../parser/tipado/esTipoPrimitivo'
import { NumeroVectorial } from '../memoria/NumeroVectorial';


export class FabricaInstrucciones {

  private instruccionesCreadas: Instruccion[]

  aInstrucciones(enunciados: Enunciado[], parametros: Declaracion[]) {
    this.instruccionesCreadas = []

    if (parametros.length > 0) {
      this.instruccionesCreadas
      for (let parametro of parametros) {
        const inicializacion = this.inicializarVariable(parametro)
        this.instruccionesCreadas = [
          ...inicializacion,
          ...this.instruccionesCreadas,
        ]
      }
    }

    for (let enunciado of enunciados) {
      this.instruccionesCreadas = [...this.instruccionesCreadas, ...this.crearInstrucciones(enunciado)]
    }

    return this.instruccionesCreadas
  }
  
  private crearInstrucciones(enunciado: Enunciado): Instruccion[] {
    switch (enunciado.tipo) {
      case 'asignacion':
        return this.crearAsignacion(enunciado)
      case 'si':
        return this.crearSi(enunciado)
      case 'mientras':
        return this.crearMientras(enunciado)
      case 'repetir':
        return this.crearRepetir(enunciado)
      case 'para':
        return this.crearPara(enunciado)
      case 'enunciado_llamada_modulo':
        return this.crearLlamadaModulo(enunciado)
      case 'retornar':
        return this.crearRetornar(enunciado)
    }
  }

  public inicializarVariable(variable: Declaracion): Instruccion[] {
    if (esTipoPrimitivo(variable.tipo_dato) == true) {
      return [new AsignarEscalar(variable.identificador)]
    } else if (variable.tipo_dato instanceof TipoMatricial) {
      return this.inicializarVariableMatricial(variable.identificador, variable.tipo_dato.dimensiones)
    }
  }

  private inicializarVariableMatricial(nombre: string, dimensiones: number[]): Instruccion[] {
    let instrucciones: Instruccion[] = []
    const contador = new NumeroVectorial(dimensiones)
    contador.setValor(dimensiones)
    while (contador.mayor([0])) {
      for (let indice of contador) {
        instrucciones.push(new Apilar(indice))
      }
      instrucciones.push(new AsignarCeldaVector(nombre, dimensiones.length))
      contador.decrementar()
    }
    return instrucciones
  }

  private crearAsignacion(asignacion: Asignacion): Instruccion[] {

    if (asignacion.variable.tipo_dato instanceof TipoMatricial) {
      // La asignacion a un vector puede ser:
      // 1 - Asignar un escalar a una celda de un vector
      // 2 - Asinar a un rango de celdas de un vector
      //  2.a - Copiar un vector a otro (vectores de la misma longitud)
      //  2.b - Copiar parte de un vector a otro (vectores de tamaños diferentes)
      //  2.c - Asignar una cadena "literal" a un vector

      // Para determinar que caso aplicar necesito conocer el tipo
      // de datos de la expresion del lado derecho
      const tipo_lado_derecho = this.extraerTipo(asignacion.expresion)

      if (esTipoPrimitivo(tipo_lado_derecho) == true) {
        // Caso 1
        return [
          ...this.crearEvaluacionExpresion(asignacion.expresion),
          ...this.crearEvaluacionIndices(asignacion.variable.indices),
          new AsignarCeldaVector(asignacion.variable.nombre, asignacion.variable.indices.length)
        ]
      } else {
        if (tipo_lado_derecho instanceof TipoMatricial) {
          if (asignacion.variable.tipo_dato.acepta(tipo_lado_derecho)) {
            // Caso 2.a
            const lado_derecho = asignacion.expresion as InvocacionVariable
            return [
              ...this.crearEvaluacionIndices(asignacion.variable.indices),
              ...this.crearEvaluacionIndices(lado_derecho.indices),
              new CopiarVector(
                asignacion.variable.nombre,
                lado_derecho.nombre,
                asignacion.variable.tipo_dato.dimensiones,
                tipo_lado_derecho.dimensiones,
                asignacion.variable.indices.length,
                lado_derecho.indices.length
              )
            ]
          }
        }

      }
    } else {
      return [
        ...this.crearEvaluacionExpresion(asignacion.expresion),
        new AsignarEscalar(asignacion.variable.nombre)
      ]
    }
  }

  private crearSi(si: Si): Instruccion[] {
    const condicion = this.crearEvaluacionExpresion(si.condicion)

    let ramaVerdadera: Instruccion[] = []
    for (let enunciado of si.rama_verdadera) {
      ramaVerdadera = [...ramaVerdadera, ...this.crearInstrucciones(enunciado)]
    }

    let ramaFalsa: Instruccion[] = []
    for (let enunciado of si.rama_falsa) {
      ramaFalsa = [...ramaFalsa, ...this.crearInstrucciones(enunciado)]
    }

    if (ramaVerdadera.length == 0 && ramaFalsa.length == 0) {
      return []
    } else {
      if (ramaVerdadera.length > 0 && ramaFalsa.length == 0) {
        const destino = this.instruccionesCreadas.length + condicion.length + ramaVerdadera.length + 1
        return [
          ...condicion,
          new SaltarSiFalso(destino),
          ...ramaVerdadera
        ]
      } else if (ramaVerdadera.length == 0 && ramaFalsa.length > 0) {
        const destino = this.instruccionesCreadas.length + condicion.length + ramaFalsa.length + 1
        return [
          ...condicion,
          new SaltarSiVerdadero(destino),
          ...ramaFalsa
        ]
      } else {
        // Si ambas ramas tienen algo, debo agregar:
        // Un salto condicional (para cuando la condicion da falso) que permita ir a la rama falsa
        // Un salto incondicional como ultimo enunciado de la rama verdadera, para saltear la rama falsa
        const inicioRamaFalsa = this.instruccionesCreadas.length + condicion.length + 1 + ramaVerdadera.length + 1
        const finRamaFalsa = inicioRamaFalsa + ramaFalsa.length
        return [
          ...condicion,
          new SaltarSiFalso(inicioRamaFalsa),
          ...ramaVerdadera,
          new Saltar(finRamaFalsa),
          ...ramaFalsa
        ]
      }
    }
  }

  private crearMientras(mientras: Mientras): Instruccion[] {
    const condicion = this.crearEvaluacionExpresion(mientras.condicion)

    let cuerpo: Instruccion[] = []
    for (let enunciado of mientras.enunciados) {
      cuerpo = [...cuerpo, ...this.crearInstrucciones(enunciado)]
    }

    // Sumo 1 dos veces por cada uno de los saltos que agrego
    const saltearBucle = this.instruccionesCreadas.length + condicion.length + 1 + cuerpo.length + 1

    const volverACondicion = this.instruccionesCreadas.length

    return [
      ...condicion,
      new SaltarSiFalso(saltearBucle),
      ...cuerpo,
      new Saltar(volverACondicion)
    ]
  }

  private crearRepetir(repetir: Repetir): Instruccion[] {
    const condicion = this.crearEvaluacionExpresion(repetir.condicion)

    let cuerpo: Instruccion[] = []
    for (let enunciado of repetir.enunciados) {
      cuerpo = [...cuerpo, ...this.crearInstrucciones(enunciado)]
    }

    const volverACondicion = this.instruccionesCreadas.length

    return [
      ...cuerpo,
      ...condicion,
      new SaltarSiFalso(volverACondicion)
    ]
  }

  private crearPara(para: Para): Instruccion[] {
    const inicializacion = this.crearInstrucciones(para.inicializador)

    const condicion = [
      ...this.crearEvaluacionExpresion(para.valor_final),
      ...this.crearEvaluacionExpresion(para.inicializador.variable),
      new Mayor(),
      ...this.crearEvaluacionExpresion(para.inicializador.variable),
      ...this.crearEvaluacionExpresion(para.valor_final),
      new Igual(),
      new Or()
    ]

    let cuerpo: Instruccion[] = []
    for (let enunciado of para.enunciados) {
      cuerpo = [...cuerpo, ...this.crearInstrucciones(enunciado)]
    }

    let incremento: Instruccion[]
    if (para.inicializador.variable.tipo_dato instanceof TipoMatricial) {
      incremento = [
        ...this.crearEvaluacionExpresion(para.inicializador.variable),
        new Apilar(1),
        new Mas(),
        ...this.crearEvaluacionIndices(para.inicializador.variable.indices),
        new AsignarCeldaVector(para.inicializador.variable.nombre, para.inicializador.variable.indices.length)
      ]
    } else {
      incremento = [
        ...this.crearEvaluacionExpresion(para.inicializador.variable),
        new Apilar(1),
        new Mas(),
        new AsignarEscalar(para.inicializador.variable.nombre)
      ]
    }

    // Sumo 1 al final para evitar caer sobre el ultimo salto que agregaré
    const volverACondicion = this.instruccionesCreadas.length + inicializacion.length

    const saltearCuerpo = volverACondicion + condicion.length + 1 + cuerpo.length + incremento.length + 1

    return [
      ...inicializacion,
      ...condicion,
      new SaltarSiFalso(saltearCuerpo),
      ...cuerpo,
      ...incremento,
      new Saltar(volverACondicion)
    ]
  }

  private crearLlamadaModulo(llamada: EnunciadoLlamadaModulo): Instruccion[] {
    if (llamada.nombre.toLowerCase() == 'escribir') {
      let argumentos: Instruccion[] = []
      for (let arg of llamada.argumentos) {
        argumentos = [...argumentos, ...this.crearEvaluacionExpresion(arg)]
      }
      return [
        ...argumentos,
        new Escribir()
      ]
    } else if (llamada.nombre.toLowerCase() == 'leer') {
      const destino = llamada.argumentos[0] as InvocacionVariable
      if (destino.tipo_dato instanceof TipoMatricial) {
        return [
          new Leer(),
          ...this.crearEvaluacionIndices(destino.indices),
          new AsignarCeldaVector(destino.nombre, destino.indices.length)
        ]
      } else {
        return [
          new Leer(),
          new AsignarEscalar(destino.nombre)
        ]
      }
    } else {
      let instrucciones: Instruccion[] = []
      for (let i = 0; i < llamada.argumentos.length; i++) {
        instrucciones = [
          ...instrucciones,
          ...this.crearEvaluacionExpresion(llamada.argumentos[i])
        ]
      }
      instrucciones.push(new LlamarModulo(llamada.nombre))
      return instrucciones
    }
  }

  private crearRetornar(retornar: Retornar): Instruccion[] {
    return [
      ...this.crearEvaluacionExpresion(retornar.valor),
      new InstruccionRetornar()
    ]
  }

  private crearEvaluacionIndices(indices: ArbolExpresion[]): Instruccion[] {
    let resultado: Instruccion[] = []
    for (let indice of indices) {
      resultado = [...resultado, ...this.crearEvaluacionExpresion(indice)]
    }
    return resultado
  }

  private crearEvaluacionExpresion(expresion: ArbolExpresion): Instruccion[] {
    const expRpn = arbol_a_rpn(expresion)

    let instrucciones: Instruccion[] = []

    for (let elemento of expRpn) {
      instrucciones = [...instrucciones, ...this.elementoAInstrucciones(elemento)]
    }

    return instrucciones
  }

  private elementoAInstrucciones(e: Operando | Operador): Instruccion[] {
    switch (e.tipo) {
      case 'operando':
        switch (e.nombre) {
          case 'literal':
            return [new Apilar(e.valor)]
          case 'invocacion':
            return this.crearInvocacion(e);
        }
      case 'operador':
        switch (e.nombre) {
          case 'and':
            return [new And()];
          case 'or':
            return [new Or()];
          case 'menor':
            return [new Menor()];
          case 'menor_igual':
            return [new MenorIgual()];
          case 'mayor':
            return [new Mayor()];
          case 'mayor_igual':
            return [new MayorIgual()];
          case 'igual':
            return [new Igual()];
          case 'diferente':
            return [new Diferente()];
          case 'mas':
            return [new Mas()];
          case 'menos':
            return [new Menos()];
          case 'por':
            return [new Por()];
          case 'barra':
            return [new DivisionReal()];
          case 'div':
            return [new DivisionEntera()];
          case 'mod':
            return [new Mod()];
          case 'elevar':
            return [new Elevar()];
          case 'negativo':
            return [new Negativo()];
          case 'not':
            return [new Not()];
        }
    }
  }

  private crearInvocacion(invocacion: Invocacion): Instruccion[] {
    if (invocacion.tipo_dato instanceof TipoMatricial) {
      if (invocacion.cantidadIndices == (invocacion.tipo_dato as TipoMatricial).dimensiones.length) {
        return [new ApilarCeldaVector(invocacion.nombreVariable, invocacion.cantidadIndices)]
      } else {
        const dimensiones = (invocacion.tipo_dato as TipoMatricial).dimensiones
        return [new ApilarCeldas(invocacion.nombreVariable, invocacion.cantidadIndices, dimensiones)]
      }
    } else {
      return [new ApilarVariable(invocacion.nombreVariable)]
    }
  }

  private extraerTipo(expresion: Expresion): TipoDato {
    if (expresion.tipo == 'operacion') {
      const tipo_dato = expresion.tipo_calculado.calcular()
      if (tipo_dato.error == false) {
        return tipo_dato.valor
      } else {
        throw new Error("NO SE DEBERIA TIRAR ESTA EXCEPCION")
      }
    } else {
      return expresion.tipo_dato
    }
  }
}