import { OperacionBinaria } from "./OperacionBinaria";

export class Menos extends OperacionBinaria<number, number> {
  constructor() {
    super((a, b) => a - b)
  }
}