import { Instruccion } from "./Instruccion";
import { NumeroVectorial } from "../memoria/NumeroVectorial";

export class CopiarVector extends Instruccion {
  private indicesLadoIzq: number;
  private indicesLadoDer: number;
  private dimensionesLadoIzq: number[]
  private dimensionesLadoDer: number[]
  private nombreLadoIzq: string
  private nombreLadoDer: string

  constructor(
    nombreLadoIzq: string,
    nombreLadoDer: string,
    dimensionesLadoIzq: number[],
    dimensionesLadoDer: number[],
    indicesLadoIzq: number,
    indicesLadoDer: number
  ) {
    super();
    this.indicesLadoIzq = indicesLadoIzq;
    this.indicesLadoDer = indicesLadoDer;
    this.dimensionesLadoIzq = dimensionesLadoIzq
    this.dimensionesLadoDer = dimensionesLadoDer
    this.nombreLadoIzq = nombreLadoIzq
    this.nombreLadoDer = nombreLadoDer
  }

  ejecutar() {
    // LD = lado derecho
    const indiceParcialLD = this.getMemoria().desapilarVarios(this.indicesLadoDer) as number[]
    const contadorLD = new NumeroVectorial(this.dimensionesLadoDer)
    contadorLD.setValor(indiceParcialLD)
    const ladoDer = this.getMemoria().recuperarVector(this.nombreLadoDer)

    // LI = lado izq
    const indiceParcialLI = this.getMemoria().desapilarVarios(this.indicesLadoIzq) as number[]
    const contadorLI = new NumeroVectorial(this.dimensionesLadoIzq)
    contadorLD.setValor(indiceParcialLI)
    const ladoIzq = this.getMemoria().recuperarVector(this.nombreLadoIzq)

    while (contadorLI.menor(this.dimensionesLadoIzq) || contadorLI.igual(this.dimensionesLadoIzq)) {
      const nuevoValor = ladoDer.obtenerCelda(contadorLD.comoArreglo()).valor()
      const celda = ladoIzq.obtenerCelda(contadorLI.comoArreglo())
      celda.asignar(nuevoValor)
      contadorLI.aumentar()
      contadorLD.aumentar()
    }
  }

  deshacer() {}
}
