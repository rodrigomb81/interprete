import { Instruccion } from './Instruccion'
import { valor_primitivo } from '../memoria/TiposMemoria';

export class Escribir extends Instruccion {
  private valor_desapilado: valor_primitivo

  ejecutar() {
    this.valor_desapilado = this.getMemoria().desapilar()
    this.getMemoria().escribirSalida(this.valor_desapilado)
  }

  deshacer() {
    // Esto es un efecto secundario...tiene sentido este metodo???
    this.getMemoria().apilar(this.valor_desapilado)
  }
}