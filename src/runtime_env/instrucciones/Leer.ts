import { Instruccion } from './Instruccion'

export class Leer extends Instruccion {

  ejecutar() {
    this.getMemoria().solicitarLectura()
  }

  deshacer() {
    // ???
  }
}