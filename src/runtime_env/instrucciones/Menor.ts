import { OperacionBinaria } from "./OperacionBinaria";

export class Menor extends OperacionBinaria<number, boolean> {
  constructor() {
    super((a, b) => a < b)
  }
}