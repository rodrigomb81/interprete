import { OperacionBinaria } from "./OperacionBinaria";

export class Diferente extends OperacionBinaria<number | boolean | string, boolean> {
  constructor() {
    super((a, b) => a != b)
  }
}