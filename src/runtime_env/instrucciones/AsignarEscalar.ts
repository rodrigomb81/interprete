import { Instruccion } from "./Instruccion";
import { valor_primitivo } from "../memoria/TiposMemoria";

export class AsignarEscalar extends Instruccion {
  private valor_previo: valor_primitivo
  private valor_nuevo: valor_primitivo
  private nombreVariable: string

  constructor(nombreVariable: string) {
    super()
    this.nombreVariable = nombreVariable
  }

  ejecutar() {
    const variable = super.getMemoria().recuperarEscalar(this.nombreVariable)
    this.valor_previo = variable.valor()
    this.valor_nuevo = super.getMemoria().desapilar()
    variable.asignar(this.valor_nuevo)
  }

  deshacer() {
    const variable = super.getMemoria().recuperarEscalar(this.nombreVariable)
    variable.asignar(this.valor_previo)
    super.getMemoria().apilar(this.valor_nuevo)
  }
}