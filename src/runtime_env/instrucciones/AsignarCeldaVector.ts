import { Instruccion } from "./Instruccion";
import { valor_primitivo } from "../memoria/TiposMemoria";

export class AsignarCeldaVector extends Instruccion {
  private valor_previo: valor_primitivo;
  private valor_nuevo: valor_primitivo;
  private nombreVariable: string;
  private cantidadIndices: number;
  private indices: number[]

  constructor(nombreVariable: string, indices: number) {
    super();
    this.nombreVariable = nombreVariable;
    this.cantidadIndices = indices;
  }

  ejecutar() {
    const variable = super.getMemoria().recuperarVector(this.nombreVariable);
    this.indices = super.getMemoria().desapilarVarios(this.cantidadIndices) as number[]
    const celda = variable.obtenerCelda(this.indices);
    this.valor_previo = celda.valor();
    this.valor_nuevo = super.getMemoria().desapilar();
    celda.asignar(this.valor_nuevo);
  }

  deshacer() {
    const variable = super.getMemoria().recuperarVector(this.nombreVariable);
    const celda = variable.obtenerCelda(this.indices);

    celda.asignar(this.valor_previo);
    super.getMemoria().apilar(this.valor_nuevo);
    super.getMemoria().apilarVarios(this.indices)
  }
}
