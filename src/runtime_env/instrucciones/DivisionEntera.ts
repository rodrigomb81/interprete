import { OperacionBinaria } from "./OperacionBinaria";

export class DivisionEntera extends OperacionBinaria<number, number> {
  constructor() {
    super((a, b) => {
      return Math.trunc(((a - (a % b)) / b))
    })
  }
}