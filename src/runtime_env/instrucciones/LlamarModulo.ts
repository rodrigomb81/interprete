import { Instruccion } from './Instruccion'

export class LlamarModulo extends Instruccion {
  private nombre: string

  constructor(nombre: string) {
    super()
    this.nombre = nombre
  }

  ejecutar() {
    this.getMemoria().llamarModulo(this.nombre)
  }

  deshacer() {}
}