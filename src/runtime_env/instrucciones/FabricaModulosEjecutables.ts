import { AST, Modulo } from "../../parser/TiposParser";
import { ModuloEjecutable } from "../ModuloEjecutable";
import { ModuloEjecutableConBreakpoints } from "../ModuloEjecutableConBreakpoints";
import { FabricaInstrucciones } from "./FabricaInstrucciones";
import { Detener } from "./Detener";
import { Retornar } from "./Retornar";
import { Instruccion } from './Instruccion';

export class FabricaModulosEjecutables {
  private fabricaInstrucciones: FabricaInstrucciones = new FabricaInstrucciones();

  private habilitarBreakpoints: boolean

  constructor(habilitarBreakpoints: boolean) {
    this.habilitarBreakpoints = habilitarBreakpoints
  }

  crearModulos(ast: AST): ModuloEjecutable[] {
    const modulos: ModuloEjecutable[] = [];

    for (let modulo of ast.modulos) {
      modulos.push(this.crearModulo(modulo))
    }

    return modulos;
  }

  private crearModulo(modulo: Modulo): ModuloEjecutable {
    let instrucciones = this.fabricaInstrucciones.aInstrucciones(modulo.enunciados, modulo.parametros)

    const locales = [...modulo.parametros, ...modulo.declaraciones]

    if (modulo.nombre == 'principal') {
      instrucciones.push(new Detener())
    } else {
      instrucciones.push(new Retornar())
    }

    if (this.habilitarBreakpoints) {
      return new ModuloEjecutableConBreakpoints(modulo.nombre, instrucciones, locales)
    } else {
      return new ModuloEjecutable(modulo.nombre, instrucciones, locales)
    }
  }
}
