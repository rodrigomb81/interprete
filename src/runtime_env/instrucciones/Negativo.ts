import { Instruccion } from './Instruccion'

export class Negativo extends Instruccion {

  ejecutar() {
    const valor = this.getMemoria().desapilar() as number
    this.getMemoria().apilar(-valor)
  }

  deshacer() {
    const valor = this.getMemoria().desapilar() as number
    this.getMemoria().apilar(-valor)
  }
}