import { ModuloEjecutable } from './ModuloEjecutable'
import { Instruccion } from './instrucciones/Instruccion'
import { Declaracion } from '../parser/TiposParser';

export class ModuloEjecutableConBreakpoints extends ModuloEjecutable {

  private breakpoints: Set<number>
  private _breakpointAlcanzado: boolean

  constructor(nombre: string, instrucciones: Instruccion[], locales: Declaracion[]) {
    super(nombre, instrucciones, locales)
    this.breakpoints = new Set()
    this._breakpointAlcanzado = false
  }

  ejecutarInstruccion() {
    const contador = this.getMemoria().getContadorInstruccion()
    if (this.breakpoints.has(contador)) {
      if (this._breakpointAlcanzado == true) {
        this._breakpointAlcanzado = false
        super.ejecutarInstruccion()
      } else {
        this._breakpointAlcanzado = true
      }
    } else {
      super.ejecutarInstruccion()
    }
  }

  agregarBreakpoint(instruccion: number) {
    this.breakpoints.add(instruccion)
  }

  quitarBreakpoint(instruccion: number) {
    this.breakpoints.delete(instruccion)
  }

  breakpointAlcanzado() {
    return this._breakpointAlcanzado
  }
}