export enum Estado {
  EJECUTANDO,
  FINALIZADO,
  LEYENDO,
  ESCRIBIENDO,
  PAUSADO
}

export enum Evento {
  LEER,
  FINALIZAR,
  ESCRIBIR,
  RESUMIR,
  BREAKPOINT_ALCANZADO
}

export class EstadoEvaluador {
  private estadoActual: Estado

  private tabla: Map<Estado, Map<Evento, Estado>> = new Map()

  constructor() {
    this.estadoActual = Estado.EJECUTANDO
    const transicionesEjecutando = new Map()
    transicionesEjecutando.set(Evento.LEER, Estado.LEYENDO)
    transicionesEjecutando.set(Evento.FINALIZAR, Estado.FINALIZADO)
    transicionesEjecutando.set(Evento.ESCRIBIR, Estado.ESCRIBIENDO)
    transicionesEjecutando.set(Evento.BREAKPOINT_ALCANZADO, Estado.PAUSADO)

    this.tabla.set(Estado.EJECUTANDO, transicionesEjecutando)

    const transicionesLeyendo = new Map()
    transicionesLeyendo.set(Evento.RESUMIR, Estado.EJECUTANDO)

    this.tabla.set(Estado.LEYENDO, transicionesLeyendo)

    const transicionesEscribiendo = new Map()
    transicionesEscribiendo.set(Evento.RESUMIR, Estado.EJECUTANDO)

    this.tabla.set(Estado.ESCRIBIENDO, transicionesEscribiendo)

    const transicionesPausado = new Map()
    transicionesPausado.set(Evento.RESUMIR, Estado.EJECUTANDO)

    this.tabla.set(Estado.PAUSADO, transicionesPausado)
  }

  transicion(e: Evento) {
    if (this.tabla.has(this.estadoActual)) {
      const fila = this.tabla.get(this.estadoActual)

      if (fila.has(e)) {
        this.estadoActual = fila.get(e)
      }
    }
  }

  valor(): Estado {
    return this.estadoActual
  }
}