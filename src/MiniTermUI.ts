import { readFileSync, existsSync } from 'fs'
import * as prompt from 'prompt-sync'

import { Compilador } from "../src/NuevoCompilador";
import { MaquinaVirtual } from "./runtime_env/MaquinaVirtual";

const compilador = new Compilador(true)

const evaluador = new MaquinaVirtual()

const lector = prompt()

if (process.argv[2]) {
  if (existsSync(process.argv[2])) {
    const codigo = readFileSync(process.argv[2] as string, 'utf8')

    const modulos = compilador.compilar(codigo)

    if (modulos.length == 0) {
      for (let error of compilador.getErrores()) {
        console.log(error)
      }
    } else {
      evaluador.setPrograma(modulos)
      evaluador.ejecutar()
      while (!evaluador.finAlcanzado()) {
        if (evaluador.hayEscrituraPendiente() || evaluador.hayLecturaPendiente()) {
          if (evaluador.hayEscrituraPendiente()) {
            console.log(evaluador.getProximaEscritura())
          } else {
            const lectura = leer()
            evaluador.enviarEntrada(lectura)
          }
        }
        evaluador.resumir()
      }
      console.log('Programa finalizado')
    }
  } else {
    console.log(`El archivo "${process.argv[2]}" no existe`)
  }
}

function leer() {
  const v = lector('>')
  if (!isNaN(Number(v))) {
    return Number(v)
  } else if (v.length > 0) {
    const b = v.toLowerCase()
    if (b == 'verdadero' || b == 'falso') {
      return b == 'verdadero'
    }
  }
  return v
}