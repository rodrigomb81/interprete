import { TipoDato } from "./TipoDato";
import { Miembro } from '../TiposParser'

export class TipoTad implements TipoDato {
  readonly nombre: string

  private miembros: Map<string, Miembro> = new Map()

  constructor(nombre: string, miembros: Miembro[]) {
    this.nombre = nombre

    for (let miembro of miembros) {
      this.miembros.set(miembro.identificador, miembro)
    }
  }

  igual(t: TipoDato): boolean {
    if (t instanceof TipoTad) {
      if (this.nombre == t.nombre) {
        return this.miembrosIguales(t.miembros)
      }
    }
    return false;
  }

  private miembrosIguales(miembrosAjenos: Map<string, Miembro>): boolean {
    let sonIguales = true

    for (let nombre in miembrosAjenos) {
      if (sonIguales && nombre in this.miembros) {
        const propio = this.miembros[nombre]

        const ajeno = miembrosAjenos[nombre]

        sonIguales = ajeno.tipo_dato.igual(propio.tipo_dato)
      }
    }
    
    return sonIguales;
  }

  acepta(t: TipoDato) {
    return this.igual(t)
  }
}