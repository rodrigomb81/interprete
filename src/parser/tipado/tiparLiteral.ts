import { TipoDato } from './TipoDato'
import { TipoCaracter } from './TipoCaracter';
import { TipoLogico } from './TipoLogico';
import { TipoCadena } from './TipoCadena';
import { TipoEntero } from './TipoEntero';
import { TipoReal } from './TipoReal';

export function tiparLiteral(valor: string | number | boolean): TipoDato {
  if (typeof valor == 'number') {
    if (valor - Math.trunc(valor) == 0) {
      return new TipoEntero()
    } else {
      return new TipoReal()
    }
  } else if (typeof valor == 'string') {
    if (valor.length == 0) {
      return new TipoCaracter()
    } else {
      return new TipoCadena(valor.length)
    }
  } else if (typeof valor == 'boolean') {
    return new TipoLogico()
  }
}