import { TipoDato } from "./TipoDato";

export class TipoEntero implements TipoDato {
  igual(t: TipoDato) {
    return t instanceof TipoEntero
  }

  acepta(t: TipoDato) {
    return t instanceof TipoEntero
  }
}