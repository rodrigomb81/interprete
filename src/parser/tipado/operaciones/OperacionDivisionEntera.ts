import { OperacionBinaria } from './OperacionBinaria'
import { Expresion } from '../../TiposParser';
import { ReglaBinariaSimple } from './ReglaBinariaSimple';
import { TipoEntero } from '../TipoEntero';

export class OperacionDivisionEntera extends OperacionBinaria {
  
  constructor(a: Expresion, b: Expresion) {
    super(
      a,
      b,
      new ReglaBinariaSimple([new TipoEntero(), new TipoEntero()], () => new TipoEntero())
    )
  }
}