import { ReglaTipado } from "./ReglaTipado";
import { TipoDato } from "../TipoDato";

export class ReglaUnariaSimple implements ReglaTipado {
  private productor: () => TipoDato;
  private aceptados: TipoDato[];

  constructor(aceptados: TipoDato[], resultado: () => TipoDato) {
    this.productor = resultado;
    this.aceptados = aceptados;
  }

  acepta(parametros: TipoDato[]): boolean {
    if (parametros.length == 1) {
      return this.aceptados[0].igual(parametros[0]);
    } else {
      return false;
    }
  }

  resultado(parametros: TipoDato[]): TipoDato {
    return this.productor()
  }

  getAceptados() {
    return [this.aceptados]
  }
}
