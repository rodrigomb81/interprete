import { Resultado } from "../../../utility/Resultado";
import { TipoDato } from "../TipoDato";

export interface TipoCalculado {
  calcular(): Resultado<TipoDato, ErrorTipado>;
}

export type ErrorTipado = ErrorTipadoOperacion | ErrorTipoIncalculable;

/**
 * Error emitido cuando una operacion recibe
 * tipos de datos que no puede utilizar.
 */
export type ErrorTipadoOperacion = {
  tipo: "error";
  nombre: "error-tipado-operacion";
  esperados: string[];
  obtenidos: string[];
};

export function crearErrorTipadoOperacion(
  esperados: string[],
  obtenidos: string[]
): ErrorTipadoOperacion {
  return {
    tipo: "error",
    nombre: "error-tipado-operacion",
    esperados,
    obtenidos
  };
}

/**
 * Error emitido cuando el tipo de datos de
 * una expresion no puede calcularse.
 */
export type ErrorTipoIncalculable = {
  tipo: "error";
  nombre: "error-tipo-incalculable";
};

export function crearErrorTipoIncalculable(): ErrorTipoIncalculable {
  return {
    tipo: "error",
    nombre: "error-tipo-incalculable"
  };
}