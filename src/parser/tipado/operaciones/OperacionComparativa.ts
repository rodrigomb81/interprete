import { OperacionBinaria } from './OperacionBinaria'
import { Expresion } from '../../TiposParser';
import { ReglaComparativa } from './ReglaComparativa'

export class OperacionComparativa extends OperacionBinaria {
  constructor(a: Expresion, b: Expresion) {
    super(
      a,
      b,
      new ReglaComparativa()
    )
  }
}