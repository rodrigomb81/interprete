import { ReglaTipado } from './ReglaTipado'
import { TipoLogico } from '../TipoLogico'
import { TipoDato } from '../TipoDato';

/**
 * Esta es una regla que toma dos tipos
 * cualesquiera, y los acepta siempre y
 * cuando sean iguales.
 */
export class ReglaComparativa implements ReglaTipado {

  acepta(parametros: TipoDato[]): boolean {
    if (parametros.length == 2) {
      return parametros[0].igual(parametros[1]);
    }
    return false;
  }

  resultado(parametros: TipoDato[]): TipoDato {
    return new TipoLogico();
  }

  // Esto se complica, porque las comparaciones (== y !=)
  // dos tipos de datos cuales quiera, siempre y cuando
  // sean iguales...
  getAceptados(): TipoDato[][] {
    return []
  }
}