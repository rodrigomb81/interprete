import { TipoDato } from './../TipoDato'
import { ReglaTipado } from './ReglaTipado'
import { ReglaUnariaSimple } from './ReglaUnariaSimple'

/**
 * Regla unaria compuesta por varias reglas simples.
 * 
 * Acepta un conjunto de parametros si alguna de sus
 * reglas los acepta.
 */
export class ReglaUnariaCompuesta implements ReglaTipado {

  private reglas: ReglaUnariaSimple[]

  constructor(reglas: ReglaUnariaSimple[]) {
    this.reglas = reglas
  }

  agregarRegla(regla: ReglaUnariaSimple) {
    this.reglas.push(regla)
  }

  acepta(parametros: TipoDato[]): boolean {
    let acepta = false;

    for (let i = 0; i < this.reglas.length && !acepta; i++) {
      acepta = this.reglas[i].acepta(parametros)
    }

    return acepta
  }

  resultado(parametros: TipoDato[]): TipoDato {
    let acepta = false;

    let reglaQueAcepta: ReglaUnariaSimple

    for (let i = 0; i < this.reglas.length && !acepta; i++) {
      acepta = this.reglas[i].acepta(parametros)

      if (acepta) {
        reglaQueAcepta = this.reglas[i]
      }
    }

    return reglaQueAcepta.resultado(parametros)
  }

  getAceptados(): TipoDato[][] {
    let resultados: TipoDato[][] = []
    for (let regla of this.reglas) {
      resultados = [...resultados, ...regla.getAceptados()]
    }
    return resultados
  }
}