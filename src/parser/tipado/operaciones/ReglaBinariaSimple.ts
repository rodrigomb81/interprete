import { ReglaTipado } from "./ReglaTipado";
import { TipoDato } from "../TipoDato";

/**
 * Representa a una regla que toma
 * dos tipos como parametro y
 * retorna otro.
 */
export class ReglaBinariaSimple implements ReglaTipado {
  private productor: () => TipoDato;
  private aceptados: TipoDato[];

  constructor(aceptados: TipoDato[], resultado: () => TipoDato) {
    this.productor = resultado;
    this.aceptados = aceptados;
  }

  acepta(parametros: TipoDato[]): boolean {
    if (parametros.length == 2) {
      const coinciden =
        this.aceptados[0].igual(parametros[0]) &&
        this.aceptados[1].igual(parametros[1]);

      return coinciden;
    } else {
      return false;
    }
  }

  resultado(parametros: TipoDato[]): TipoDato {
    return this.productor()
  }

  getAceptados() {
    return [this.aceptados]
  }
}
