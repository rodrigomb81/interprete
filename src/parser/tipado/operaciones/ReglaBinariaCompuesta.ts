import { TipoDato } from './../TipoDato'
import { ReglaTipado } from './ReglaTipado'
import { ReglaBinariaSimple } from './ReglaBinariaSimple'

/**
 * Regla binaria compuesta por varias reglas simples.
 * 
 * Acepta un conjunto de parametros si alguna de sus
 * reglas los acepta.
 */
export class ReglaBinariaCompuesta implements ReglaTipado {

  private reglas: ReglaBinariaSimple[]

  constructor(reglas: ReglaBinariaSimple[]) {
    this.reglas = reglas
  }

  agregarRegla(regla: ReglaBinariaSimple) {
    this.reglas.push(regla)
  }

  acepta(parametros: TipoDato[]): boolean {
    let acepta = false;

    for (let i = 0; i < this.reglas.length && !acepta; i++) {
      acepta = this.reglas[i].acepta(parametros)
    }

    return acepta
  }

  resultado(parametros: TipoDato[]): TipoDato {
    let acepta = false;

    let reglaQueAcepta: ReglaBinariaSimple

    for (let i = 0; i < this.reglas.length && !acepta; i++) {
      acepta = this.reglas[i].acepta(parametros)

      if (acepta) {
        reglaQueAcepta = this.reglas[i]
      }
    }

    return reglaQueAcepta.resultado(parametros)
  }

  getAceptados(): TipoDato[][] {
    let resultados: TipoDato[][] = []
    for (let regla of this.reglas) {
      resultados = [...resultados, ...regla.getAceptados()]
    }
    return resultados
  }
}