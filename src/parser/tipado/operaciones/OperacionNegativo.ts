import { OperacionUnaria } from './OperacionUnaria'
import { ReglaUnariaSimple } from './ReglaUnariaSimple'
import { ReglaUnariaCompuesta } from './ReglaUnariaCompuesta'
import { Expresion } from '../../TiposParser';
import { TipoEntero } from '../TipoEntero';
import { TipoReal } from '../TipoReal';

export class OperacionNegativo extends OperacionUnaria {
  constructor(a: Expresion) {
    super(
      a,
      new ReglaUnariaCompuesta([
        new ReglaUnariaSimple([new TipoEntero()], () => new TipoEntero()),
        new ReglaUnariaSimple([new TipoReal()], () => new TipoReal())
      ])
    )
  }
}