import { OperacionUnaria } from './OperacionUnaria'
import { ReglaUnariaSimple } from './ReglaUnariaSimple'
import { Expresion } from '../../TiposParser';
import { TipoLogico } from '../TipoLogico';

export class OperacionNot extends OperacionUnaria {
  constructor(a: Expresion) {
    super(
      a,
      new ReglaUnariaSimple([new TipoLogico()], () => new TipoLogico())
    )
  }
}