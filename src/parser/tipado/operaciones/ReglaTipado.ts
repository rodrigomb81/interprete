import { TipoDato } from "../TipoDato";

export interface ReglaTipado {

  /**
   * Dice si esta regla puede aplicarse
   * a los tipos dados.
   * 
   * @param parametros
   */
  acepta(parametros: TipoDato[]): boolean

  /**
   * Dice cual es el tipo resultante dados
   * los parametros recibidos.
   * 
   * @param parametros 
   */
  resultado(parametros: TipoDato[]): TipoDato

  /**
   * Retorna todos los parametros aceptados
   * por esta regla.
   */
  getAceptados(): TipoDato[][]
}