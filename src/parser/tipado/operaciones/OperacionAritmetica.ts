import { OperacionBinaria } from "./OperacionBinaria";
import { ReglaOrBinaria } from "./ReglaOrBinaria";
import { Expresion } from "../../TiposParser";
import { TipoEntero } from "../TipoEntero";
import { TipoReal } from "../TipoReal";

/**
 * Representa a las operaciones de tipado para
 * las siguientes:
 *    suma
 *    resta
 *    multiplicacion
 *    exponenciacion
 */
export class OperacionAritmetica extends OperacionBinaria {
  constructor(a: Expresion, b: Expresion) {
    super(
      a,
      b,
      new ReglaOrBinaria(new TipoEntero(), new TipoReal())
    )
  }
}