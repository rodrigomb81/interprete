import { Expresion } from '../../TiposParser'
import { TipoLogico } from "../TipoLogico";
import { OperacionBinaria } from './OperacionBinaria';
import { ReglaBinariaSimple } from './ReglaBinariaSimple'

/**
 * Representa las operaciones AND y OR
 */
export class OperacionLogica extends OperacionBinaria {

  constructor(a: Expresion, b: Expresion) {
    super(
      a,
      b,
      new ReglaBinariaSimple(
        [new TipoLogico(), new TipoLogico()],
        () => new TipoLogico
      )
    )
  }
}
