import { OperacionBinaria } from './OperacionBinaria'
import { ReglaBinariaCompuesta } from './ReglaBinariaCompuesta'
import { ReglaBinariaSimple } from './ReglaBinariaSimple'
import { Expresion } from '../../TiposParser';
import { TipoEntero } from '../TipoEntero';
import { TipoLogico } from '../TipoLogico';
import { TipoReal } from '../TipoReal';

export class OperacionRelacional extends OperacionBinaria {
  
  constructor(a: Expresion, b: Expresion) {
    super(
      a,
      b,
      new ReglaBinariaCompuesta([
        new ReglaBinariaSimple([new TipoEntero(), new TipoEntero()], () => new TipoLogico),
        new ReglaBinariaSimple([new TipoEntero(), new TipoReal()], () => new TipoLogico),
        new ReglaBinariaSimple([new TipoReal(), new TipoEntero()], () => new TipoLogico),
        new ReglaBinariaSimple([new TipoReal(), new TipoReal()], () => new TipoLogico)
      ])
    )
  }
}