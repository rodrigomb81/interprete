import { OperacionBinaria } from './OperacionBinaria'
import { ReglaBinariaCompuesta } from './ReglaBinariaCompuesta';
import { Expresion } from '../../TiposParser';
import { ReglaBinariaSimple } from './ReglaBinariaSimple';
import { TipoEntero } from '../TipoEntero';
import { TipoReal } from '../TipoReal';

export class OperacionDivisionReal extends OperacionBinaria {
  
  constructor(a: Expresion, b: Expresion) {
    super(
      a,
      b,
      new ReglaBinariaCompuesta([
        new ReglaBinariaSimple([new TipoEntero(), new TipoEntero()], () => new TipoReal()),
        new ReglaBinariaSimple([new TipoEntero(), new TipoReal()], () => new TipoReal()),
        new ReglaBinariaSimple([new TipoReal(), new TipoEntero()], () => new TipoReal()),
        new ReglaBinariaSimple([new TipoReal(), new TipoReal()], () => new TipoReal())
      ])
    )
  }
}