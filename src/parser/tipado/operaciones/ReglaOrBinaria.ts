import { ReglaTipado } from "./ReglaTipado";
import { TipoDato } from "../TipoDato";

/**
 * Representa una operacion con tipos de datos
 * cuyo tipo resultante es analogo al OR booleano.
 * 
 * Por ejemplo, la suma. El tipo de dato del
 * resultado de una suma se comporta de la
 * siguiente manera:
 * 
 * entero + entero  = entero
 * entero + real    = real
 * real   + entero  = real
 * real   + real    = real
 * 
 * Es decir, solo basta que uno de los operandos
 * sea real para que el resultado sea real.
 */ 
export class ReglaOrBinaria implements ReglaTipado {
  private aceptados: TipoDato[]
  private cero: TipoDato
  private uno: TipoDato

  constructor(cero: TipoDato, uno: TipoDato) {
    this.aceptados = [cero, uno]
    this.cero = cero
    this.uno = uno
  }

  acepta(parametros: TipoDato[]): boolean {
    if (parametros.length == 2) {
      if (parametros[0].igual(this.cero) || parametros[0].igual(this.uno)) {
        if ((parametros[1].igual(this.cero) || parametros[1].igual(this.uno))) {
          return true
        }
      }
    }
    return false
  }

  resultado(parametros: TipoDato[]): TipoDato {
    if ((parametros[0].igual(this.cero) && parametros[1].igual(this.cero))) {
      return this.cero
    } else {
      return this.uno
    }
  }

  getAceptados() {
    return [this.aceptados]
  }
}