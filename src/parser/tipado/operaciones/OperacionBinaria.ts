import { Expresion } from '../../TiposParser';
import { OperacionAbstracta } from './OperacionAbstracta'
import { Resultado } from "../../../utility/Resultado";
import { TipoCalculado, ErrorTipado, crearErrorTipadoOperacion, crearErrorTipoIncalculable } from './TipoCalculado'
import { ReglaTipado } from './ReglaTipado'
import { TipoDato } from '../TipoDato';

export class OperacionBinaria extends OperacionAbstracta implements TipoCalculado {
  private a: Expresion
  private b: Expresion;
  private regla: ReglaTipado

  constructor(a: Expresion, b: Expresion, regla: ReglaTipado) {
    super()
    this.a = a;
    this.b = b;
    this.regla = regla;
  }

  
  calcular(): Resultado<TipoDato, ErrorTipado> {
    const tipo_a = this.extraerTipo(this.a)

    const tipo_b = this.extraerTipo(this.b)

    if (tipo_a.existe == true && tipo_b.existe == true) {
      const parametros = [tipo_a.valor, tipo_b.valor]
      if (this.regla.acepta(parametros)) {
        return { error: false, valor: this.regla.resultado(parametros) }
      } else {
        const esperados = this.regla.getAceptados()[0].map(o => o.toString())
        const obtenidos = [tipo_a.valor.toString(), tipo_b.valor.toString()]
        return {
          error: true,
          valor: crearErrorTipadoOperacion(esperados, obtenidos)
        }
      }
    } else {
      return { error: true, valor: crearErrorTipoIncalculable() }
    }
  }
}