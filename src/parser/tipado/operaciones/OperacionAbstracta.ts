import { Expresion } from '../../TiposParser'
import { Opcional } from "../../../utility/Opcional";
import { TipoDato } from "../TipoDato";

export abstract class OperacionAbstracta {

  /**
   * Obtiene el tipo de datos de una expresion.
   * 
   * Una expresion puede ser una operacion o un
   * operando. Los operandos tienen un tipo de
   * datos comun y corriente, pero el de las
   * expresiones debe ser calculado (y el calculo
   * puede fallar).
   * 
   * @param e expresion cuyo tipo se quiere obtener
   */
  extraerTipo(e: Expresion): Opcional<TipoDato> {
    if (e.tipo == 'operacion') {
      const tipo_dato = e.tipo_calculado.calcular()

      if (tipo_dato.error == true)  {
        return { existe: false, valor: null }
      } else {
        return { existe: true, valor: tipo_dato.valor }
      }
    } else {
      return { existe: true, valor: e.tipo_dato }
    }
  }
}