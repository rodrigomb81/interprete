import { TipoDato } from "./TipoDato";
import { Declaracion } from '../TiposParser'

export class TipoModulo implements TipoDato {
  readonly nombre: string

  readonly tipoRetorno: TipoDato
  
  private tiposParametros: TipoDato[]

  constructor(nombre: string, parametros: Declaracion[], retorno: TipoDato) {
    this.nombre = nombre
    this.tipoRetorno = retorno
    this.tiposParametros = parametros.map(p => p.tipo_dato)
  }

  igual(t: TipoDato): boolean {
    if (t instanceof TipoModulo) {
      if (t.tipoRetorno.igual(this.tipoRetorno)) {
        return this.parametrosIguales(t.tiposParametros)
      }
    }
    return false
  }

  /**
   * Compara un conjunto de tipos de datos contro los tipos de los parametros
   * de este modulo.
   * 
   * Es util cuando hace falta ver si una serie de argumentos puede usarse
   * para invocar un modulo.
   * 
   * @param tipos tipos a comparar
   */
  parametrosIguales(tipos: TipoDato[]): boolean {
    let sonIguales = true
    if (tipos.length == this.tiposParametros.length) {
      for (let i = 0; i < tipos.length && sonIguales; i++) {
        const ajeno = tipos[i]
        const propio = this.tiposParametros[i]
        sonIguales = propio.igual(ajeno)
      }
    } else {
      sonIguales = false;
    }
    return sonIguales
  }

  acepta(t: TipoDato) {
    return this.igual(t)
  }
}