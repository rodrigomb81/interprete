import { TipoDato } from "./TipoDato";

export class TipoMatricial implements TipoDato {
  readonly encapsula: TipoDato
  readonly dimensiones: number[]

  constructor(encapsulado: TipoDato, dimension: number[]) {
    if (encapsulado instanceof TipoMatricial) {
      let e: TipoDato = encapsulado
      let d = dimension.slice(0)
      while (e instanceof TipoMatricial) {
        d = [...e.dimensiones, ...d]
        e = e.encapsula
      }
      this.encapsula = e
      this.dimensiones = d
    } else {
      this.encapsula = encapsulado
      this.dimensiones = dimension
    }
  }

  igual(t: TipoDato): boolean {
    if (t instanceof TipoMatricial) {
      if (t.encapsula.igual(this.encapsula)) {
        if (this.dimensiones.length == t.dimensiones.length) {
          let dimensionesIguales = true
          for (let i = 0; i < this.dimensiones.length; i++) {
            dimensionesIguales = this.dimensiones[i] == t.dimensiones[i]
          }
          return dimensionesIguales
        }
      }
    }
    return false
  }

  acepta(t: TipoDato): boolean {
    // Por ahora solo se acepta un tipo matricial de la misma dimension
    // y que encapsule el mismo tipo de datos.
    //
    // Esto podria mejorarse. Por ejemplo, un vector de reales deberia
    // poder aceptar un vector de enteros.
    return this.igual(t)
  }
}