export interface TipoDato {
  /**
   * Compara este tipo de datos con otro y dice si son
   * iguales.
   * @param t tipo a comparar
   */
  igual(t: TipoDato): boolean

  /**
   * Verifica si este tipo de datos acepta valores del
   * tipo dado.
   * 
   * Por ejemplo: un valor de tipo real acepta valores
   * de tipo entero, pero un valor de tipo logico no.
   * @param t tipo a verificar
   */
  acepta(t: TipoDato): boolean
}