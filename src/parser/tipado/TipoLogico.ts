import { TipoDato } from "./TipoDato";

export class TipoLogico implements TipoDato {
  igual(t: TipoDato) {
    return t instanceof TipoLogico
  }

  acepta(t: TipoDato) {
    return t instanceof TipoLogico
  }
}