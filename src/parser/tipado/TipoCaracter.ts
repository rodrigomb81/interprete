import { TipoDato } from "./TipoDato";

export class TipoCaracter implements TipoDato {
  igual(t: TipoDato) {
    return t instanceof TipoCaracter
  }

  acepta(t: TipoDato) {
    return t instanceof TipoCaracter
  }
}