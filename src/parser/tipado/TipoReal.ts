import { TipoDato } from "./TipoDato";
import { TipoEntero } from './TipoEntero'

export class TipoReal implements TipoDato {
  igual(t: TipoDato): boolean {
    return t instanceof TipoReal
  }

  acepta(t: TipoDato): boolean {
    return t instanceof TipoReal || t instanceof TipoEntero
  }
}