import { TipoDato } from './TipoDato'
import { TipoEntero } from './TipoEntero'
import { TipoReal } from './TipoReal'
import { TipoCaracter } from './TipoCaracter'
import { TipoLogico } from './TipoLogico'

export function esTipoPrimitivo(t: TipoDato): t is (TipoEntero | TipoReal | TipoCaracter | TipoLogico) {
  return (
    t instanceof TipoEntero ||
    t instanceof TipoReal ||
    t instanceof TipoCaracter ||
    t instanceof TipoLogico
  );
}