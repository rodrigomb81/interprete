import { TipoMatricial } from './TipoMatricial'
import { TipoCaracter } from './TipoCaracter';

export class TipoCadena extends TipoMatricial {
  constructor(longitud: number) {
    super(new TipoCaracter(), [longitud])
  }
}