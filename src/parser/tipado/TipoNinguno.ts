import { TipoDato } from "./TipoDato";

export class TipoNinguno implements TipoDato {

  igual(t: TipoDato): boolean {
    return false
  }

  acepta(t: TipoDato) {
    return false
  }
}