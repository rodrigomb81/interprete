import { TipoDato } from "./TipoDato";

export class TipoReferencial implements TipoDato {
  readonly encapsula: TipoDato

  constructor(encapsulado: TipoDato) {
    this.encapsula = encapsulado
  }

  igual(t: TipoDato): boolean {
    if (t instanceof TipoReferencial) {
      return t.encapsula.igual(this.encapsula)
    }
    return false
  }

  acepta(t: TipoDato): boolean {
    return this.igual(t)
  }
}