import {
  Declaracion,
  Modulo,
  TipoAbstracto,
  InvocacionVariable
} from "./TiposParser";
import { Opcional } from "../utility/Opcional";
import { TipoDato } from "./tipado/TipoDato";
import { TipoModulo } from "./tipado/TipoModulo";
import { TipoNinguno } from "./tipado/TipoNinguno";

export type SimboloInexistente =
  | VariableInexistente
  | ModuloInexistente
  | TADInexistente;

export type VariableInexistente = {
  tipo: "variable-inexistente";
  nombre: string;
};

export type ModuloInexistente = {
  tipo: "modulo-inexistente";
  nombre: string;
};

export type TADInexistente = {
  tipo: "tad-inexistente";
  nombre: string;
};

export type ColisionVariable = {
  tipo: "colision-variable";
  original: Declaracion;
  duplicado: Declaracion;
};

export class TablaSimbolos {
  private modulos: Map<string, Modulo> = new Map();

  private consultasTipadoPendientes: InvocacionVariable[] = [];

  private consultasPostergadas: InvocacionVariable[] = []

  private variables: Map<string, Map<string, Declaracion>> = new Map();

  private errores: any[] = [];

  constructor() {
    this.modulos.set("escribir", {
      nombre: "escribir",
      enunciados: [],
      declaraciones: [],
      parametros: [],
      tipo: "modulo",
      tipo_dato: new TipoModulo('escribir', [], new TipoNinguno())
    });
    this.modulos.set("leer", {
      nombre: "leer",
      enunciados: [],
      declaraciones: [],
      parametros: [],
      tipo: "modulo",
      tipo_dato: new TipoModulo('leer', [], new TipoNinguno())
    });
  }

  resetear() {
    this.modulos = new Map();
    this.consultasTipadoPendientes = [];
    this.consultasPostergadas = []
    // this.variables = new Map()
    this.errores = [];
    this.modulos.set("escribir", {
      nombre: "escribir",
      enunciados: [],
      declaraciones: [],
      parametros: [],
      tipo: "modulo",
      tipo_dato: new TipoModulo('escribir', [], new TipoNinguno())
    });
    this.modulos.set("leer", {
      nombre: "leer",
      enunciados: [],
      declaraciones: [],
      parametros: [],
      tipo: "modulo",
      tipo_dato: new TipoModulo('leer', [], new TipoNinguno())
    });
  }

  getErrores() {
    return this.errores;
  }

  finalizar() {
    for (let consulta of this.consultasPostergadas) {
      if (!this.modulos.has(consulta.nombre)) {
        const error: VariableInexistente = {
          tipo: "variable-inexistente",
          nombre: consulta.nombre
        };
        this.errores.push(error);
      }
    }
  }

  /**
   * Declara un modulo.
   *
   * Se revisa que no exista un modulo con el mismo nombre
   * y se procesa la cola de variables a declarar.
   *
   * @param modulo modulo a declarar.
   */
  declararModulo(modulo: Modulo) {
    const declaradas: Map<string, Declaracion> = new Map();

    for (let declaracion of modulo.declaraciones) {
      this.declararVariable(declaradas, declaracion);
    }

    for (let declaracion of modulo.parametros) {
      this.declararVariable(declaradas, declaracion);
    }

    for (let i = 0; i < this.consultasTipadoPendientes.length; i++) {
      const invocacion = this.consultasTipadoPendientes[i]
      if (declaradas.has(invocacion.nombre)) {
        invocacion.tipo_dato = declaradas.get(invocacion.nombre).tipo_dato
      }
      else {
        this.consultasPostergadas.push(invocacion)
      }
    }

    this.consultasTipadoPendientes = []
  }

  private responderConsultasTipado(
    variables: Map<string, Declaracion>,
    invocacion: InvocacionVariable
  ) {
    // Responder a las consultas sobre el tipo de dato de las variables
    const nombre = invocacion.nombre;
    if (variables.has(nombre)) {
      const { tipo_dato } = variables.get(nombre);
      invocacion.tipo_dato = tipo_dato;
    } else {
      const error: VariableInexistente = {
        tipo: "variable-inexistente",
        nombre
      };
      this.errores.push(error);
    }
  }

  /**
   * Declara una variable.
   *
   * Las variables declaradas se ponen en cola, y son procesadas
   * una vez que el modulo al cual pertenecen es declarado.
   *
   * Durante el procesado se verifica que no haya mas de una variable
   * con el mismo nombre. De ser asi se emite un error.
   */
  declararVariable(
    declaradas: Map<string, Declaracion>,
    nuevaDeclaracion: Declaracion
  ) {
    if (!declaradas.has(nuevaDeclaracion.identificador)) {
      declaradas.set(nuevaDeclaracion.identificador, nuevaDeclaracion);
    } else {
      const error: ColisionVariable = {
        tipo: "colision-variable",
        duplicado: nuevaDeclaracion,
        original: declaradas.get(nuevaDeclaracion.identificador)
      };
      this.errores.push(error);
    }
  }

  /**
   * Declara un tipo abstracto de datos.
   *
   * ????
   *
   * @param tad tad a declarar
   */
  declararTad(tad: TipoAbstracto) {}

  /**
   * Retorna el tipo de dato de una variable.
   *
   * Si la variable solicitada no existe, guarda un error.
   *
   * @param nombre nombre de la variable buscada
   */
  tiparInvocacion(invocacion: InvocacionVariable) {
    this.consultasTipadoPendientes.push(invocacion);
  }

  /**
   * Retorna el tipo de dato de un modulo.
   *
   * Si el modulo no existe, guarda un error.
   *
   * @param nombre nombre del modulo buscado
   */
  tipoModulo(nombre: string): Opcional<TipoDato> {
    if (nombre.toLowerCase() == "escribir" || nombre.toLowerCase() == "leer") {
      return {
        existe: true,
        valor: new TipoModulo(nombre, [], new TipoNinguno())
      };
    } else if (this.modulos.has(nombre)) {
      const m = this.modulos.get(nombre);
      return { existe: true, valor: m.tipo_dato };
    } else {
      const error: ModuloInexistente = {
        tipo: "modulo-inexistente",
        nombre
      };
      this.errores.push(error);
      return { existe: false, valor: null };
    }
  }
}
