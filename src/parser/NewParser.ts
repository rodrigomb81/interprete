import { Parser as NearleyParser, Grammar, LexerState } from 'nearley'
import * as gramatica from './gramatica'
import { Resultado } from '../utility/Resultado'
import { AST } from './TiposParser';
import { TablaSimbolos } from './TablaSimbolos';

export class Parser {
  /**
   * Parser que lee un programa entero.
   */
  private parserPrograma: NearleyParser
  private estadoInicialParserPrograma: {[key: string]: any, lexerState: LexerState}

  /**
   * Parser que lee una sola expresion.
   */
  private parserExpresion: NearleyParser

  /**
   * Parser que lee una expresion literal.
   */
  private parserLiteral: NearleyParser

  tabla: TablaSimbolos

  constructor() {
    // Gramaticas cargadas del mismo archivo, pero que parten de producciones diferentes.
    const produccionPrograma  = Grammar.fromCompiled(gramatica)
    const produccionExpresion = Grammar.fromCompiled(gramatica)
    const produccionLiteral   = Grammar.fromCompiled(gramatica)
    this.tabla = gramatica.tabla

    // produccionExpresion.start = 'expresion'

    // produccionLiteral.start = 'literal'

    this.parserPrograma = new NearleyParser(produccionPrograma)
    this.parserExpresion = new NearleyParser(produccionExpresion)
    this.parserLiteral = new NearleyParser(produccionLiteral)

    this.estadoInicialParserPrograma = this.parserPrograma.save()
  }

  /**
   * Metodo que analiza un programa.
   */
  analizar(cadena: string): Resultado<AST, any[]> {
    try {
      // Es necesario resetear el parser a mano
      // porque, por algun motivo, nearley no
      // lo hace.
      // El motivo es: el parser esta hecho para
      // recibir datos de manera incremental.
      // La idea es que uno llame feed y vaya
      // tomando los resultados ¯\_(ツ)_/¯

      this.parserPrograma.restore(this.estadoInicialParserPrograma)

      this.tabla.resetear()

      const resultados = this.parserPrograma.feed(cadena).results

      // if (this.tabla.getErrores().length > 0) {
      //   return { error: true, result: this.tabla.getErrores() }
      // } else {
      //   return { error: false, result: resultados[0] as AST }
      // }
      return { error: false, valor: resultados[0] as AST }
    
    } catch (e) {
      const error = {
        tipo: 'error-de-sintaxis'
      }

      return {error: true, valor: [error, ...this.tabla.getErrores()]  }
    }
  }
}