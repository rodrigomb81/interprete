// Generated automatically by nearley, version 2.15.1
// http://github.com/Hardmath123/nearley
// Bypasses TS6133. Allow declared but unused functions.
// @ts-ignore
function id(d: any[]): any { return d[0]; }
declare var tipo: any;
declare var identificador: any;
declare var fin_tipo: any;
declare var espacio: any;
declare var variables: any;
declare var nl: any;
declare var inicio: any;
declare var fin: any;
declare var par_izq: any;
declare var par_der: any;
declare var coma: any;
declare var fin_funcion: any;
declare var fin_procedimiento: any;
declare var separador: any;
declare var ref: any;
declare var cor_izq: any;
declare var cor_der: any;
declare var repetir: any;
declare var hasta: any;
declare var mientras: any;
declare var fin_mientras: any;
declare var para: any;
declare var fin_para: any;
declare var si: any;
declare var entonces: any;
declare var sino: any;
declare var fin_si: any;
declare var retornar: any;
declare var asignador: any;
declare var and: any;
declare var or: any;
declare var menor: any;
declare var menor_igual: any;
declare var mayor: any;
declare var mayor_igual: any;
declare var igual: any;
declare var diferente: any;
declare var mas: any;
declare var menos: any;
declare var por: any;
declare var barra: any;
declare var div: any;
declare var mod: any;
declare var elevar: any;
declare var not: any;
declare var accesor: any;
declare var entero: any;
declare var real: any;
declare var logico: any;
declare var cadena: any;

const moo = require("moo");

import { TipoMatricial } from './tipado/TipoMatricial'
import { TipoReferencial } from './tipado/TipoReferencial'
import { TipoModulo } from './tipado/TipoModulo'
import { TipoReal } from './tipado/TipoReal'
import { TipoEntero } from './tipado/TipoEntero'
import { TipoLogico } from './tipado/TipoLogico'
import { TipoCaracter } from './tipado/TipoCaracter'
import { TipoCadena } from './tipado/TipoCadena'
import { TipoNinguno } from './tipado/TipoNinguno'
import { tiparLiteral } from './tipado/tiparLiteral'
import { OperacionAritmetica } from './tipado/operaciones/OperacionAritmetica'
import { OperacionLogica } from './tipado/operaciones/OperacionLogica'
import { OperacionRelacional } from './tipado/operaciones/OperacionRelacional'
import { OperacionComparativa } from './tipado/operaciones/OperacionComparativa'
import { OperacionDivisionReal } from './tipado/operaciones/OperacionDivisionReal'
import { OperacionDivisionEntera } from './tipado/operaciones/OperacionDivisionEntera'
import { OperacionNegativo } from './tipado/operaciones/OperacionNegativo'
import { OperacionNot } from './tipado/operaciones/OperacionNot'

import { TablaSimbolos } from './TablaSimbolos'

const lexer = moo.compile({
  cadena:             /"(?:\\["bfnrt\/\\]|\\u[a-fA-F0-9]{4}|[^"\\])*"/,
  sino:               /sino/,
  si:                 /si/,
  entonces:           /entonces/,
  fin_procedimiento:  /fin *procedimiento/,
  fin_mientras:       /fin *mientras/,
  fin_funcion:        /fin *funcion/,
  fin_para:           /fin *para/,
  fin_tipo:           /fin *tipo/,
  fin_si:             /fin *si/,
  fin:                /fin/,
  tipo:               /tipo/,
  inicio:             /inicio/,
  para:               /para/,
  variables:          /variables/,
  ref:                /ref/,
  repetir:            /repetir/,
  retornar:           /retornar/,
  hasta:              /hasta/,
  mientras:           /mientras/,
  separador:          / *: */,
  mas:                / *\+ */,
  accesor:            / *-> */,
  menos:              / *- */,
  not:                / *! */,
  elevar:             / *\*\* */,
  por:                / *\* */,
  barra:              / *\/ */,
  div:                / *div */,
  mod:                / +mod +/,
  and:                / *&& */,
  or:                 / *\|\| */,
  asignador:          / *<- */,
  menor_igual:        / *<= */,
  menor:              / *< */,
  mayor_igual:        / *>= */,
  mayor:              / *> */,
  igual:              / *== */,
  diferente:          / *!= */,
  real:               /[0-9]+\.[0-9]+/,
  entero:             /[0-9]+/,
  logico:             /falso|verdadero/,
  par_izq:            /\( */,
  par_der:            / *\)/,
  cor_izq:            /\[ */,
  cor_der:            / *\]/,
  identificador:      /[_a-zA-Z]+/,
  coma:               / *, */,
  espacio:            / +/,
  nl:                 { match: /\n+/, lineBreaks: true },
});

function flatten(arr: any[]): any[] {
  return arr.reduce((a, b) => [...a, ...flatten_if_arr(b)], [])
}

function flatten_if_arr(i: any): any[] {
  return i instanceof Array ? flatten(i):[i];
}

function nuller(): null {
  return null
}

export const tabla = new TablaSimbolos();

export interface Token { value: any; [key: string]: any };

export interface Lexer {
  reset: (chunk: string, info: any) => void;
  next: () => Token | undefined;
  save: () => any;
  formatError: (token: Token) => string;
  has: (tokenType: string) => boolean
};

export interface NearleyRule {
  name: string;
  symbols: NearleySymbol[];
  postprocess?: (d: any[], loc?: number, reject?: {}) => any
};

export type NearleySymbol = string | { literal: any } | { test: (token: any) => boolean };

export var Lexer: Lexer | undefined = lexer;

export var ParserRules: NearleyRule[] = [
    {"name": "sigma", "symbols": ["programa"], "postprocess":  data => {
          tabla.finalizar()
          return data[0]
        } },
    {"name": "programa$ebnf$1", "symbols": []},
    {"name": "programa$ebnf$1", "symbols": ["programa$ebnf$1", "tad"], "postprocess": (d) => d[0].concat([d[1]])},
    {"name": "programa$ebnf$2", "symbols": ["fl"], "postprocess": id},
    {"name": "programa$ebnf$2", "symbols": [], "postprocess": () => null},
    {"name": "programa$ebnf$3", "symbols": []},
    {"name": "programa$ebnf$3", "symbols": ["programa$ebnf$3", "eb"], "postprocess": (d) => d[0].concat([d[1]])},
    {"name": "programa", "symbols": ["programa$ebnf$1", "principal", "programa$ebnf$2", "programa$ebnf$3"], "postprocess":  data => {
          return {
            tipo: 'programa',
            modulos: [ data[1] ],
            tads: data[0] == null ? []:data[0]
          }
        } },
    {"name": "programa$ebnf$4", "symbols": []},
    {"name": "programa$ebnf$4", "symbols": ["programa$ebnf$4", "tad"], "postprocess": (d) => d[0].concat([d[1]])},
    {"name": "programa$ebnf$5", "symbols": []},
    {"name": "programa$ebnf$5", "symbols": ["programa$ebnf$5", "eb"], "postprocess": (d) => d[0].concat([d[1]])},
    {"name": "programa", "symbols": ["programa$ebnf$4", "principal", "fl", "programa$ebnf$5", "modulos"], "postprocess":  data => {
            return {
              tipo: 'programa',
              modulos: [ data[1], ...data[4] ],
              tads: data[0] == null ? []:data[0]
            }
        } },
    {"name": "tad", "symbols": ["tipo", "nombre_tipo", "miembros", "fin_tipo"], "postprocess":  data => {
          return {
            tipo: 'tad',
            nombre: data[1],
            miembros: data[2].map((miembro: {}) => { return { ...miembro, tad_padre: data[1] } })
          }
        } },
    {"name": "tipo$ebnf$1", "symbols": ["il"], "postprocess": id},
    {"name": "tipo$ebnf$1", "symbols": [], "postprocess": () => null},
    {"name": "tipo$ebnf$2", "symbols": []},
    {"name": "tipo$ebnf$2", "symbols": ["tipo$ebnf$2", "eb"], "postprocess": (d) => d[0].concat([d[1]])},
    {"name": "tipo", "symbols": ["tipo$ebnf$1", (lexer.has("tipo") ? {type: "tipo"} : tipo), "fl", "tipo$ebnf$2"], "postprocess": nuller},
    {"name": "nombre_tipo$ebnf$1", "symbols": ["il"], "postprocess": id},
    {"name": "nombre_tipo$ebnf$1", "symbols": [], "postprocess": () => null},
    {"name": "nombre_tipo$ebnf$2", "symbols": []},
    {"name": "nombre_tipo$ebnf$2", "symbols": ["nombre_tipo$ebnf$2", "eb"], "postprocess": (d) => d[0].concat([d[1]])},
    {"name": "nombre_tipo", "symbols": ["nombre_tipo$ebnf$1", (lexer.has("identificador") ? {type: "identificador"} : identificador), "fl", "nombre_tipo$ebnf$2"], "postprocess": data => data[1].toString(0)},
    {"name": "miembros", "symbols": ["declaraciones"], "postprocess":  data => data[0].map((declaracion: {}) => {
          return { ...declaracion, tipo: 'declaracion_miembro' }
        }) },
    {"name": "fin_tipo$ebnf$1", "symbols": ["il"], "postprocess": id},
    {"name": "fin_tipo$ebnf$1", "symbols": [], "postprocess": () => null},
    {"name": "fin_tipo$ebnf$2", "symbols": []},
    {"name": "fin_tipo$ebnf$2", "symbols": ["fin_tipo$ebnf$2", "eb"], "postprocess": (d) => d[0].concat([d[1]])},
    {"name": "fin_tipo", "symbols": ["fin_tipo$ebnf$1", (lexer.has("fin_tipo") ? {type: "fin_tipo"} : fin_tipo), "fl", "fin_tipo$ebnf$2"], "postprocess": nuller},
    {"name": "principal$ebnf$1", "symbols": ["declaraciones"], "postprocess": id},
    {"name": "principal$ebnf$1", "symbols": [], "postprocess": () => null},
    {"name": "principal$ebnf$2", "symbols": ["enunciados"], "postprocess": id},
    {"name": "principal$ebnf$2", "symbols": [], "postprocess": () => null},
    {"name": "principal", "symbols": ["variables", "principal$ebnf$1", "inicio", "principal$ebnf$2", "fin"], "postprocess":  data => {
          const declaraciones = data[1] == null ? []:flatten(data[1])
          const principal = {
            tipo: 'modulo',
            nombre: 'principal',
            enunciados: data[3] == null ? []:flatten(data[3]),
            declaraciones: data[1] == null ? []:flatten(data[1]),
            parametros: [],
            tipo_dato: new TipoModulo('principal', [], new TipoNinguno())
          }
        
          tabla.declararModulo(principal as any)
        
          return principal
        } },
    {"name": "modulos$ebnf$1", "symbols": ["fl"], "postprocess": id},
    {"name": "modulos$ebnf$1", "symbols": [], "postprocess": () => null},
    {"name": "modulos$ebnf$2", "symbols": []},
    {"name": "modulos$ebnf$2", "symbols": ["modulos$ebnf$2", "eb"], "postprocess": (d) => d[0].concat([d[1]])},
    {"name": "modulos", "symbols": ["modulo", "modulos$ebnf$1", "modulos$ebnf$2"], "postprocess": data => [ data[0] ]},
    {"name": "modulos", "symbols": ["modulo", "fl", "modulos"], "postprocess": data => [ data[0], ...data[2] ]},
    {"name": "modulo", "symbols": ["funcion"], "postprocess": id},
    {"name": "modulo", "symbols": ["procedimiento"], "postprocess": id},
    {"name": "variables$ebnf$1", "symbols": [(lexer.has("espacio") ? {type: "espacio"} : espacio)], "postprocess": id},
    {"name": "variables$ebnf$1", "symbols": [], "postprocess": () => null},
    {"name": "variables$ebnf$2", "symbols": [(lexer.has("espacio") ? {type: "espacio"} : espacio)], "postprocess": id},
    {"name": "variables$ebnf$2", "symbols": [], "postprocess": () => null},
    {"name": "variables", "symbols": ["variables$ebnf$1", (lexer.has("variables") ? {type: "variables"} : variables), "variables$ebnf$2", (lexer.has("nl") ? {type: "nl"} : nl)], "postprocess": nuller},
    {"name": "inicio$ebnf$1", "symbols": [(lexer.has("espacio") ? {type: "espacio"} : espacio)], "postprocess": id},
    {"name": "inicio$ebnf$1", "symbols": [], "postprocess": () => null},
    {"name": "inicio$ebnf$2", "symbols": [(lexer.has("espacio") ? {type: "espacio"} : espacio)], "postprocess": id},
    {"name": "inicio$ebnf$2", "symbols": [], "postprocess": () => null},
    {"name": "inicio", "symbols": ["inicio$ebnf$1", (lexer.has("inicio") ? {type: "inicio"} : inicio), "inicio$ebnf$2", (lexer.has("nl") ? {type: "nl"} : nl)], "postprocess": nuller},
    {"name": "fin$ebnf$1", "symbols": [(lexer.has("espacio") ? {type: "espacio"} : espacio)], "postprocess": id},
    {"name": "fin$ebnf$1", "symbols": [], "postprocess": () => null},
    {"name": "fin$ebnf$2", "symbols": [(lexer.has("espacio") ? {type: "espacio"} : espacio)], "postprocess": id},
    {"name": "fin$ebnf$2", "symbols": [], "postprocess": () => null},
    {"name": "fin", "symbols": ["fin$ebnf$1", (lexer.has("fin") ? {type: "fin"} : fin), "fin$ebnf$2"], "postprocess": nuller},
    {"name": "funcion$ebnf$1", "symbols": ["declaraciones"], "postprocess": id},
    {"name": "funcion$ebnf$1", "symbols": [], "postprocess": () => null},
    {"name": "funcion$ebnf$2", "symbols": ["enunciados"], "postprocess": id},
    {"name": "funcion$ebnf$2", "symbols": [], "postprocess": () => null},
    {"name": "funcion", "symbols": ["expresion_tipo", "nombre_modulo", "lista_parametros", "funcion$ebnf$1", "inicio", "funcion$ebnf$2", "fin_funcion"], "postprocess":  data => {
          const funcion = {
            tipo: 'modulo',
            nombre: data[1],
            enunciados: data[5] == null ? []:flatten(data[5]),
            declaraciones: data[3] == null ? []:flatten(data[3]),
            parametros: data[2],
            tipo_dato: new TipoModulo(data[1], data[2], data[0])
          }
        
          tabla.declararModulo(funcion as any)
        
          return funcion
        } },
    {"name": "procedimiento$ebnf$1", "symbols": ["declaraciones"], "postprocess": id},
    {"name": "procedimiento$ebnf$1", "symbols": [], "postprocess": () => null},
    {"name": "procedimiento$ebnf$2", "symbols": ["enunciados"], "postprocess": id},
    {"name": "procedimiento$ebnf$2", "symbols": [], "postprocess": () => null},
    {"name": "procedimiento", "symbols": ["nombre_modulo", "lista_parametros", "procedimiento$ebnf$1", "inicio", "procedimiento$ebnf$2", "fin_procedimiento"], "postprocess":  data => {
          const procedimiento = {
            tipo: 'modulo',
            nombre: data[0],
            enunciados: data[4] == null ? []:flatten(data[4]),
            declaraciones: data[2] == null ? []:flatten(data[2]),
            parametros: data[1],
            tipo_dato: new TipoModulo(data[0], data[1], new TipoNinguno())
          }
        
          tabla.declararModulo(procedimiento as any)
        
          return procedimiento
        } },
    {"name": "nombre_modulo$ebnf$1", "symbols": [(lexer.has("espacio") ? {type: "espacio"} : espacio)], "postprocess": id},
    {"name": "nombre_modulo$ebnf$1", "symbols": [], "postprocess": () => null},
    {"name": "nombre_modulo$ebnf$2", "symbols": [(lexer.has("espacio") ? {type: "espacio"} : espacio)], "postprocess": id},
    {"name": "nombre_modulo$ebnf$2", "symbols": [], "postprocess": () => null},
    {"name": "nombre_modulo", "symbols": ["nombre_modulo$ebnf$1", (lexer.has("identificador") ? {type: "identificador"} : identificador), "nombre_modulo$ebnf$2"], "postprocess": data => data[1].toString(0)},
    {"name": "lista_parametros", "symbols": [(lexer.has("par_izq") ? {type: "par_izq"} : par_izq), "parametros", (lexer.has("par_der") ? {type: "par_der"} : par_der)], "postprocess": data => data[1]},
    {"name": "parametros$ebnf$1", "symbols": [(lexer.has("espacio") ? {type: "espacio"} : espacio)], "postprocess": id},
    {"name": "parametros$ebnf$1", "symbols": [], "postprocess": () => null},
    {"name": "parametros", "symbols": ["parametros$ebnf$1", "declaracion"], "postprocess": data => [ data[1] ]},
    {"name": "parametros$ebnf$2", "symbols": [(lexer.has("espacio") ? {type: "espacio"} : espacio)], "postprocess": id},
    {"name": "parametros$ebnf$2", "symbols": [], "postprocess": () => null},
    {"name": "parametros", "symbols": ["parametros$ebnf$2", "declaracion", (lexer.has("coma") ? {type: "coma"} : coma), "parametros"], "postprocess": data => [ data[1], ...data[3] ]},
    {"name": "fin_funcion$ebnf$1", "symbols": [(lexer.has("espacio") ? {type: "espacio"} : espacio)], "postprocess": id},
    {"name": "fin_funcion$ebnf$1", "symbols": [], "postprocess": () => null},
    {"name": "fin_funcion$ebnf$2", "symbols": [(lexer.has("espacio") ? {type: "espacio"} : espacio)], "postprocess": id},
    {"name": "fin_funcion$ebnf$2", "symbols": [], "postprocess": () => null},
    {"name": "fin_funcion", "symbols": ["fin_funcion$ebnf$1", (lexer.has("fin_funcion") ? {type: "fin_funcion"} : fin_funcion), "fin_funcion$ebnf$2"], "postprocess": nuller},
    {"name": "fin_procedimiento$ebnf$1", "symbols": [(lexer.has("espacio") ? {type: "espacio"} : espacio)], "postprocess": id},
    {"name": "fin_procedimiento$ebnf$1", "symbols": [], "postprocess": () => null},
    {"name": "fin_procedimiento$ebnf$2", "symbols": [(lexer.has("espacio") ? {type: "espacio"} : espacio)], "postprocess": id},
    {"name": "fin_procedimiento$ebnf$2", "symbols": [], "postprocess": () => null},
    {"name": "fin_procedimiento", "symbols": ["fin_procedimiento$ebnf$1", (lexer.has("fin_procedimiento") ? {type: "fin_procedimiento"} : fin_procedimiento), "fin_procedimiento$ebnf$2"], "postprocess": nuller},
    {"name": "declaraciones$ebnf$1", "symbols": [(lexer.has("espacio") ? {type: "espacio"} : espacio)], "postprocess": id},
    {"name": "declaraciones$ebnf$1", "symbols": [], "postprocess": () => null},
    {"name": "declaraciones", "symbols": ["declaraciones$ebnf$1", (lexer.has("nl") ? {type: "nl"} : nl)], "postprocess": data => []},
    {"name": "declaraciones$ebnf$2", "symbols": [(lexer.has("espacio") ? {type: "espacio"} : espacio)], "postprocess": id},
    {"name": "declaraciones$ebnf$2", "symbols": [], "postprocess": () => null},
    {"name": "declaraciones", "symbols": ["declaraciones$ebnf$2", "declaracion", (lexer.has("nl") ? {type: "nl"} : nl)], "postprocess":  data => {
          // tabla.declararVariable(data[1] as any)
          return [ data[1] ]
        } },
    {"name": "declaraciones$ebnf$3", "symbols": [(lexer.has("espacio") ? {type: "espacio"} : espacio)], "postprocess": id},
    {"name": "declaraciones$ebnf$3", "symbols": [], "postprocess": () => null},
    {"name": "declaraciones", "symbols": ["declaraciones$ebnf$3", "declaracion", (lexer.has("coma") ? {type: "coma"} : coma), "declaraciones"], "postprocess":  data => {
          // const declaraciones = [ data[1], ...data[3] ]
        
          /*
          for (let declaracion of declaraciones) {
            tabla.declararVariable(declaracion as any)
          }
          */
        
          return [ data[1], ...data[3] ]
        } },
    {"name": "declaracion", "symbols": [(lexer.has("identificador") ? {type: "identificador"} : identificador), (lexer.has("separador") ? {type: "separador"} : separador), "expresion_tipo"], "postprocess":  data => {
          const declaracion = {
            tipo: 'declaracion',
            identificador: data[0].toString(),
            tipo_dato: data[2]
          }
        
          return declaracion
        } },
    {"name": "expresion_tipo$ebnf$1", "symbols": [(lexer.has("espacio") ? {type: "espacio"} : espacio)], "postprocess": id},
    {"name": "expresion_tipo$ebnf$1", "symbols": [], "postprocess": () => null},
    {"name": "expresion_tipo", "symbols": [(lexer.has("ref") ? {type: "ref"} : ref), "expresion_tipo$ebnf$1", "tipo_matricial"], "postprocess": data => new TipoReferencial(data[2])},
    {"name": "expresion_tipo", "symbols": ["tipo_matricial"], "postprocess": id},
    {"name": "tipo_matricial", "symbols": ["tipo_entre_parentesis", (lexer.has("cor_izq") ? {type: "cor_izq"} : cor_izq), "indices_enteros", (lexer.has("cor_der") ? {type: "cor_der"} : cor_der)], "postprocess": data => new TipoMatricial(data[0], data[2])},
    {"name": "tipo_matricial", "symbols": ["tipo_entre_parentesis"], "postprocess": id},
    {"name": "tipo_entre_parentesis", "symbols": [(lexer.has("par_izq") ? {type: "par_izq"} : par_izq), "expresion_tipo", (lexer.has("par_der") ? {type: "par_der"} : par_der)], "postprocess": data => data[1]},
    {"name": "tipo_entre_parentesis", "symbols": ["tipo_atomico"], "postprocess": id},
    {"name": "tipo_atomico", "symbols": [(lexer.has("identificador") ? {type: "identificador"} : identificador)], "postprocess":  data => {
          const nombre = data[0].toString()
          switch (nombre.toLowerCase()) {
            case 'entero':
              return new TipoEntero()
            case 'real':
              return new TipoReal();
            case 'logico':
              return new TipoLogico();
            case 'caracter':
              return new TipoCaracter();
            default:
              // Si se llega a este caso, se trada del nombre de un TAD
              return new TipoNinguno();
          }
        } },
    {"name": "indices_enteros", "symbols": ["entero_literal"], "postprocess": data => [ data[0] ]},
    {"name": "indices_enteros", "symbols": ["entero_literal", (lexer.has("coma") ? {type: "coma"} : coma), "indices_enteros"], "postprocess": data => [ data[0], ...data[2] ]},
    {"name": "enunciados$ebnf$1", "symbols": [(lexer.has("espacio") ? {type: "espacio"} : espacio)], "postprocess": id},
    {"name": "enunciados$ebnf$1", "symbols": [], "postprocess": () => null},
    {"name": "enunciados", "symbols": ["enunciados$ebnf$1", "enunciado", (lexer.has("nl") ? {type: "nl"} : nl)], "postprocess": data => [ data[1] ]},
    {"name": "enunciados$ebnf$2", "symbols": [(lexer.has("espacio") ? {type: "espacio"} : espacio)], "postprocess": id},
    {"name": "enunciados$ebnf$2", "symbols": [], "postprocess": () => null},
    {"name": "enunciados", "symbols": ["enunciados$ebnf$2", "enunciado", (lexer.has("nl") ? {type: "nl"} : nl), "enunciados"], "postprocess": data => [ data[1], ...data[3] ]},
    {"name": "enunciado", "symbols": ["asignacion"], "postprocess": id},
    {"name": "enunciado", "symbols": ["enunciado_llamada_modulo"], "postprocess": id},
    {"name": "enunciado", "symbols": ["repetir"], "postprocess": id},
    {"name": "enunciado", "symbols": ["mientras"], "postprocess": id},
    {"name": "enunciado", "symbols": ["para"], "postprocess": id},
    {"name": "enunciado", "symbols": ["si"], "postprocess": id},
    {"name": "enunciado", "symbols": ["retornar"], "postprocess": id},
    {"name": "repetir$ebnf$1", "symbols": [(lexer.has("espacio") ? {type: "espacio"} : espacio)], "postprocess": id},
    {"name": "repetir$ebnf$1", "symbols": [], "postprocess": () => null},
    {"name": "repetir$ebnf$2", "symbols": [(lexer.has("espacio") ? {type: "espacio"} : espacio)], "postprocess": id},
    {"name": "repetir$ebnf$2", "symbols": [], "postprocess": () => null},
    {"name": "repetir$ebnf$3", "symbols": [(lexer.has("espacio") ? {type: "espacio"} : espacio)], "postprocess": id},
    {"name": "repetir$ebnf$3", "symbols": [], "postprocess": () => null},
    {"name": "repetir", "symbols": [(lexer.has("repetir") ? {type: "repetir"} : repetir), "repetir$ebnf$1", (lexer.has("nl") ? {type: "nl"} : nl), "enunciados", "repetir$ebnf$2", (lexer.has("hasta") ? {type: "hasta"} : hasta), "repetir$ebnf$3", "expresion"], "postprocess":  data => { 
          return {
            tipo: 'repetir',
            enunciados: data[3],
            condicion: data[7]
          }
        } },
    {"name": "mientras$ebnf$1", "symbols": [(lexer.has("espacio") ? {type: "espacio"} : espacio)], "postprocess": id},
    {"name": "mientras$ebnf$1", "symbols": [], "postprocess": () => null},
    {"name": "mientras$ebnf$2", "symbols": [(lexer.has("espacio") ? {type: "espacio"} : espacio)], "postprocess": id},
    {"name": "mientras$ebnf$2", "symbols": [], "postprocess": () => null},
    {"name": "mientras$ebnf$3", "symbols": [(lexer.has("espacio") ? {type: "espacio"} : espacio)], "postprocess": id},
    {"name": "mientras$ebnf$3", "symbols": [], "postprocess": () => null},
    {"name": "mientras", "symbols": [(lexer.has("mientras") ? {type: "mientras"} : mientras), "mientras$ebnf$1", "expresion", "mientras$ebnf$2", (lexer.has("nl") ? {type: "nl"} : nl), "enunciados", "mientras$ebnf$3", (lexer.has("fin_mientras") ? {type: "fin_mientras"} : fin_mientras)], "postprocess":  data => {
          return {
            tipo: 'mientras',
            enunciados: data[5],
            condicion: data[2]
          }
        } },
    {"name": "para$ebnf$1", "symbols": ["enunciados"], "postprocess": id},
    {"name": "para$ebnf$1", "symbols": [], "postprocess": () => null},
    {"name": "para", "symbols": [(lexer.has("para") ? {type: "para"} : para), "inicializador", "hasta", "valor_final", "para$ebnf$1", "fin_para"], "postprocess":  data => { 
          let enunciados = []
        
          if (data[4]) {
            enunciados = data[4]
          }
        
          return {
            tipo: 'para',
            inicializador: data[1],
            valor_final: data[3],
            enunciados
          }
        } },
    {"name": "inicializador$ebnf$1", "symbols": [(lexer.has("espacio") ? {type: "espacio"} : espacio)], "postprocess": id},
    {"name": "inicializador$ebnf$1", "symbols": [], "postprocess": () => null},
    {"name": "inicializador$ebnf$2", "symbols": [(lexer.has("espacio") ? {type: "espacio"} : espacio)], "postprocess": id},
    {"name": "inicializador$ebnf$2", "symbols": [], "postprocess": () => null},
    {"name": "inicializador", "symbols": ["inicializador$ebnf$1", "asignacion", "inicializador$ebnf$2"], "postprocess": data => data[1]},
    {"name": "valor_final$ebnf$1", "symbols": [(lexer.has("espacio") ? {type: "espacio"} : espacio)], "postprocess": id},
    {"name": "valor_final$ebnf$1", "symbols": [], "postprocess": () => null},
    {"name": "valor_final", "symbols": ["expresion", "valor_final$ebnf$1", (lexer.has("nl") ? {type: "nl"} : nl)], "postprocess": id},
    {"name": "fin_para$ebnf$1", "symbols": [(lexer.has("espacio") ? {type: "espacio"} : espacio)], "postprocess": id},
    {"name": "fin_para$ebnf$1", "symbols": [], "postprocess": () => null},
    {"name": "fin_para", "symbols": ["fin_para$ebnf$1", (lexer.has("fin_para") ? {type: "fin_para"} : fin_para)], "postprocess": nuller},
    {"name": "hasta$ebnf$1", "symbols": [(lexer.has("espacio") ? {type: "espacio"} : espacio)], "postprocess": id},
    {"name": "hasta$ebnf$1", "symbols": [], "postprocess": () => null},
    {"name": "hasta", "symbols": [(lexer.has("hasta") ? {type: "hasta"} : hasta), "hasta$ebnf$1"], "postprocess": nuller},
    {"name": "si$ebnf$1", "symbols": ["enunciados"], "postprocess": id},
    {"name": "si$ebnf$1", "symbols": [], "postprocess": () => null},
    {"name": "si$ebnf$2", "symbols": ["enunciados"], "postprocess": id},
    {"name": "si$ebnf$2", "symbols": [], "postprocess": () => null},
    {"name": "si", "symbols": [(lexer.has("si") ? {type: "si"} : si), "condicion", "entonces", "si$ebnf$1", "sino", "si$ebnf$2", "fin_si"], "postprocess":  data => {
            let rama_verdadera = []
            let rama_falsa = []
        
            if (data[3]) {
              rama_verdadera = data[3]
            }
        
            if (data[5]) {
              rama_falsa = data[5]
            }
        
            return {
              tipo: 'si',
              rama_verdadera,
              rama_falsa,
              condicion: data[1]
            }
        } },
    {"name": "si$ebnf$3", "symbols": ["enunciados"], "postprocess": id},
    {"name": "si$ebnf$3", "symbols": [], "postprocess": () => null},
    {"name": "si", "symbols": [(lexer.has("si") ? {type: "si"} : si), "condicion", "entonces", "si$ebnf$3", "fin_si"], "postprocess":  data => {
          let rama_verdadera = []
        
          if (data[3]) {
            rama_verdadera = data[3]
          }
        
          return {
            tipo: 'si',
            rama_verdadera,
            rama_falsa:[],
            condicion: data[1]
          }
        } },
    {"name": "condicion$ebnf$1", "symbols": [(lexer.has("espacio") ? {type: "espacio"} : espacio)], "postprocess": id},
    {"name": "condicion$ebnf$1", "symbols": [], "postprocess": () => null},
    {"name": "condicion$ebnf$2", "symbols": [(lexer.has("espacio") ? {type: "espacio"} : espacio)], "postprocess": id},
    {"name": "condicion$ebnf$2", "symbols": [], "postprocess": () => null},
    {"name": "condicion", "symbols": ["condicion$ebnf$1", "expresion", "condicion$ebnf$2"], "postprocess": data => data[1]},
    {"name": "entonces$ebnf$1", "symbols": [(lexer.has("espacio") ? {type: "espacio"} : espacio)], "postprocess": id},
    {"name": "entonces$ebnf$1", "symbols": [], "postprocess": () => null},
    {"name": "entonces", "symbols": [(lexer.has("entonces") ? {type: "entonces"} : entonces), "entonces$ebnf$1", (lexer.has("nl") ? {type: "nl"} : nl)], "postprocess": nuller},
    {"name": "sino$ebnf$1", "symbols": [(lexer.has("espacio") ? {type: "espacio"} : espacio)], "postprocess": id},
    {"name": "sino$ebnf$1", "symbols": [], "postprocess": () => null},
    {"name": "sino$ebnf$2", "symbols": [(lexer.has("espacio") ? {type: "espacio"} : espacio)], "postprocess": id},
    {"name": "sino$ebnf$2", "symbols": [], "postprocess": () => null},
    {"name": "sino", "symbols": ["sino$ebnf$1", (lexer.has("sino") ? {type: "sino"} : sino), "sino$ebnf$2", (lexer.has("nl") ? {type: "nl"} : nl)], "postprocess": nuller},
    {"name": "fin_si$ebnf$1", "symbols": [(lexer.has("espacio") ? {type: "espacio"} : espacio)], "postprocess": id},
    {"name": "fin_si$ebnf$1", "symbols": [], "postprocess": () => null},
    {"name": "fin_si$ebnf$2", "symbols": [(lexer.has("espacio") ? {type: "espacio"} : espacio)], "postprocess": id},
    {"name": "fin_si$ebnf$2", "symbols": [], "postprocess": () => null},
    {"name": "fin_si", "symbols": ["fin_si$ebnf$1", (lexer.has("fin_si") ? {type: "fin_si"} : fin_si), "fin_si$ebnf$2"], "postprocess": nuller},
    {"name": "retornar$ebnf$1", "symbols": [(lexer.has("espacio") ? {type: "espacio"} : espacio)], "postprocess": id},
    {"name": "retornar$ebnf$1", "symbols": [], "postprocess": () => null},
    {"name": "retornar", "symbols": [(lexer.has("retornar") ? {type: "retornar"} : retornar), "retornar$ebnf$1", "expresion"], "postprocess": data => { return { tipo: 'retornar', valor: data[2] } }},
    {"name": "asignacion", "symbols": ["invocacion", (lexer.has("asignador") ? {type: "asignador"} : asignador), "expresion"], "postprocess":  data => {
          return { tipo: 'asignacion', variable: data[0], expresion: data[2] }
        } },
    {"name": "expresion", "symbols": ["relacional"], "postprocess": id},
    {"name": "relacional", "symbols": ["relacional", (lexer.has("and") ? {type: "and"} : and), "aditiva"], "postprocess":  data => {
          return {
            tipo: 'operacion',
            subtipo: 'and',
            aridad: 'binaria',
            a: data[0],
            b: data[2],
            tipo_calculado: new OperacionLogica(data[0], data[2])
          }
        } },
    {"name": "relacional", "symbols": ["relacional", (lexer.has("or") ? {type: "or"} : or), "aditiva"], "postprocess":  data => {
            return {
              tipo: 'operacion',
              subtipo: 'or',
              aridad: 'binaria',
              a: data[0],
              b: data[2],
              tipo_calculado: new OperacionLogica(data[0], data[2])
            }
        } },
    {"name": "relacional", "symbols": ["relacional", (lexer.has("menor") ? {type: "menor"} : menor), "aditiva"], "postprocess":  data => {
            return {
              tipo: 'operacion',
              subtipo: 'menor',
              aridad: 'binaria',
              a: data[0],
              b: data[2],
              tipo_calculado: new OperacionRelacional(data[0], data[2])
            }
        } },
    {"name": "relacional", "symbols": ["relacional", (lexer.has("menor_igual") ? {type: "menor_igual"} : menor_igual), "aditiva"], "postprocess":  data => {
            return {
              tipo: 'operacion',
              subtipo: 'menor_igual',
              aridad: 'binaria',
              a: data[0],
              b: data[2],
              tipo_calculado: new OperacionRelacional(data[0], data[2])
            }
        } },
    {"name": "relacional", "symbols": ["relacional", (lexer.has("mayor") ? {type: "mayor"} : mayor), "aditiva"], "postprocess":  data => {
            return {
              tipo: 'operacion',
              subtipo: 'mayor',
              aridad: 'binaria',
              a: data[0],
              b: data[2],
              tipo_calculado: new OperacionRelacional(data[0], data[2])
            }
        } },
    {"name": "relacional", "symbols": ["relacional", (lexer.has("mayor_igual") ? {type: "mayor_igual"} : mayor_igual), "aditiva"], "postprocess":  data => {
            return {
              tipo: 'operacion',
              subtipo: 'mayor_igual',
              aridad: 'binaria',
              a: data[0],
              b: data[2],
              tipo_calculado: new OperacionRelacional(data[0], data[2])
            }
        } },
    {"name": "relacional", "symbols": ["relacional", (lexer.has("igual") ? {type: "igual"} : igual), "aditiva"], "postprocess":  data => {
            return {
              tipo: 'operacion',
              subtipo: 'igual',
              aridad: 'binaria',
              a: data[0],
              b: data[2],
              tipo_calculado: new OperacionComparativa(data[0], data[2])
            }
        } },
    {"name": "relacional", "symbols": ["relacional", (lexer.has("diferente") ? {type: "diferente"} : diferente), "aditiva"], "postprocess":  data => {
            return {
              tipo: 'operacion',
              subtipo: 'diferente',
              aridad: 'binaria',
              a: data[0],
              b: data[2],
              tipo_calculado: new OperacionComparativa(data[0], data[2])
            }
        } },
    {"name": "relacional", "symbols": ["aditiva"], "postprocess": id},
    {"name": "aditiva", "symbols": ["aditiva", (lexer.has("mas") ? {type: "mas"} : mas), "multiplicativa"], "postprocess":  data => {
          return {
            tipo: 'operacion',
            subtipo: 'mas',
            aridad: 'binaria',
            a: data[0],
            b: data[2],
            tipo_calculado: new OperacionAritmetica(data[0], data[2])
          }
        } },
    {"name": "aditiva", "symbols": ["aditiva", (lexer.has("menos") ? {type: "menos"} : menos), "multiplicativa"], "postprocess":  data => {
            return {
              tipo: 'operacion',
              subtipo: 'menos',
              aridad: 'binaria',
              a: data[0],
              b: data[2],
              tipo_calculado: new OperacionAritmetica(data[0], data[2])
            }
        } },
    {"name": "aditiva", "symbols": ["multiplicativa"], "postprocess": id},
    {"name": "multiplicativa", "symbols": ["multiplicativa", (lexer.has("por") ? {type: "por"} : por), "entre_parentensis"], "postprocess":  data => {
          return {
            tipo: 'operacion',
            subtipo: 'por',
            aridad: 'binaria',
            a: data[0],
            b: data[2],
            tipo_calculado: new OperacionAritmetica(data[0], data[2])
          }
        } },
    {"name": "multiplicativa", "symbols": ["multiplicativa", (lexer.has("barra") ? {type: "barra"} : barra), "entre_parentensis"], "postprocess":  data => {
          return {
            tipo: 'operacion',
            subtipo: 'barra',
            aridad: 'binaria',
            a: data[0],
            b: data[2],
            tipo_calculado: new OperacionDivisionReal(data[0], data[2])
          }
        } },
    {"name": "multiplicativa", "symbols": ["multiplicativa", (lexer.has("div") ? {type: "div"} : div), "entre_parentensis"], "postprocess":  data => {
          return {
            tipo: 'operacion',
            subtipo: 'div',
            aridad: 'binaria',
            a: data[0],
            b: data[2],
            tipo_calculado: new OperacionDivisionEntera(data[0], data[2])
          }
        } },
    {"name": "multiplicativa", "symbols": ["multiplicativa", (lexer.has("mod") ? {type: "mod"} : mod), "entre_parentensis"], "postprocess":  data => {
          return {
            tipo: 'operacion',
            subtipo: 'mod',
            aridad: 'binaria',
            a: data[0],
            b: data[2],
            tipo_calculado: new OperacionDivisionEntera(data[0], data[2])
          }
        } },
    {"name": "multiplicativa", "symbols": ["exponencial"], "postprocess": id},
    {"name": "exponencial", "symbols": ["exponencial", (lexer.has("elevar") ? {type: "elevar"} : elevar), "unaria"], "postprocess":  data => {
          return {
            tipo: 'operacion',
            subtipo: 'elevar',
            aridad: 'binaria',
            a: data[0],
          b: data[2],
            tipo_calculado: new OperacionAritmetica(data[0], data[2])
          }
        } },
    {"name": "exponencial", "symbols": ["unaria"], "postprocess": id},
    {"name": "unaria", "symbols": [(lexer.has("menos") ? {type: "menos"} : menos), "entre_parentensis"], "postprocess":  data => {
          return {
            tipo: 'operacion',
            subtipo: 'negativo',
            aridad: 'unaria',
            a: data[1],
            tipo_calculado: new OperacionNegativo(data[1])
          }
        } },
    {"name": "unaria", "symbols": [(lexer.has("not") ? {type: "not"} : not), "entre_parentensis"], "postprocess":  data => {
          return {
            tipo: 'operacion',
            subtipo: 'not',
            aridad: 'unaria',
            a: data[1],
            tipo_calculado: new OperacionNot(data[1])
          }
        } },
    {"name": "unaria", "symbols": ["entre_parentensis"], "postprocess": id},
    {"name": "entre_parentensis", "symbols": [(lexer.has("par_izq") ? {type: "par_izq"} : par_izq), "expresion", (lexer.has("par_der") ? {type: "par_der"} : par_der)], "postprocess": data => data[1]},
    {"name": "entre_parentensis", "symbols": ["atomica"], "postprocess": id},
    {"name": "atomica", "symbols": ["literal"], "postprocess": id},
    {"name": "atomica", "symbols": ["invocacion"], "postprocess": id},
    {"name": "atomica", "symbols": ["llamada_modulo"], "postprocess": id},
    {"name": "literal", "symbols": ["entero_literal"], "postprocess":  data => { 
        return { tipo: 'operando', subtipo: 'literal', valor: data[0], tipo_dato: tiparLiteral(data[0]) } }
            },
    {"name": "literal", "symbols": ["real_literal"], "postprocess":  data => { 
        return { tipo: 'operando', subtipo: 'literal', valor: data[0], tipo_dato: tiparLiteral(data[0]) } }
          },
    {"name": "literal", "symbols": ["cadena_literal"], "postprocess":  data => { 
        return { tipo: 'operando', subtipo: 'literal', valor: data[0], tipo_dato: tiparLiteral(data[0]) } }
          },
    {"name": "literal", "symbols": ["logico_literal"], "postprocess":  data => { 
        return { tipo: 'operando', subtipo: 'literal', valor: data[0], tipo_dato: tiparLiteral(data[0]) } }
          },
    {"name": "invocacion", "symbols": ["invocacion_miembro"], "postprocess": id},
    {"name": "invocacion", "symbols": ["invocacion_variable"], "postprocess": id},
    {"name": "invocacion_miembro", "symbols": ["invocacion_variable", (lexer.has("accesor") ? {type: "accesor"} : accesor), "invocacion_variable"], "postprocess":  data => {
          return { tipo: 'miembro', padre: data[0], miembro: data[2] }
        } },
    {"name": "invocacion_miembro", "symbols": ["invocacion_variable", (lexer.has("accesor") ? {type: "accesor"} : accesor), "invocacion_miembro"], "postprocess":  data => {
          return { tipo: 'miembro', padre: data[0], miembro: data[2] }
        } },
    {"name": "invocacion_variable", "symbols": [(lexer.has("identificador") ? {type: "identificador"} : identificador), (lexer.has("cor_izq") ? {type: "cor_izq"} : cor_izq), "indices", (lexer.has("cor_der") ? {type: "cor_der"} : cor_der)], "postprocess":  data => {
          const invocacion = { tipo: 'operando', subtipo: 'variable', nombre: data[0].toString(), indices: data[2], tipo_dato: null }
          tabla.tiparInvocacion(invocacion as any)
          return invocacion
        } },
    {"name": "invocacion_variable", "symbols": [(lexer.has("identificador") ? {type: "identificador"} : identificador)], "postprocess":  data => {
          const invocacion = { tipo: 'operando', subtipo: 'variable', nombre: data[0].toString(), indices: [], tipo_dato: null }
          tabla.tiparInvocacion(invocacion as any)
          return invocacion
        } },
    {"name": "indices", "symbols": ["expresion"]},
    {"name": "indices", "symbols": ["expresion", (lexer.has("coma") ? {type: "coma"} : coma), "indices"], "postprocess": data => [ data[0], ...data[2] ]},
    {"name": "llamada_modulo", "symbols": [(lexer.has("identificador") ? {type: "identificador"} : identificador), (lexer.has("par_izq") ? {type: "par_izq"} : par_izq), "argumentos", (lexer.has("par_der") ? {type: "par_der"} : par_der)], "postprocess":  data => {
          const tipo_dato = tabla.tipoModulo(data[0].toString())
          return { tipo: 'operando', subtipo: 'llamada_modulo', nombre: data[0].toString(), argumentos: data[2], tipo_dato }
        } },
    {"name": "llamada_modulo", "symbols": [(lexer.has("identificador") ? {type: "identificador"} : identificador), (lexer.has("par_izq") ? {type: "par_izq"} : par_izq), (lexer.has("par_der") ? {type: "par_der"} : par_der)], "postprocess":  data => {
          const tipo_dato = tabla.tipoModulo(data[0].toString())
          return { tipo: 'operando', subtipo: 'llamada_modulo', nombre: data[0].toString(), argumentos: [], tipo_dato }
        } },
    {"name": "enunciado_llamada_modulo", "symbols": [(lexer.has("identificador") ? {type: "identificador"} : identificador), (lexer.has("par_izq") ? {type: "par_izq"} : par_izq), "argumentos", (lexer.has("par_der") ? {type: "par_der"} : par_der)], "postprocess":  data => {
          const tipo_dato = tabla.tipoModulo(data[0].toString())
          return { tipo: 'enunciado_llamada_modulo', nombre: data[0].toString(), argumentos: data[2], tipo_dato }
        } },
    {"name": "enunciado_llamada_modulo", "symbols": [(lexer.has("identificador") ? {type: "identificador"} : identificador), (lexer.has("par_izq") ? {type: "par_izq"} : par_izq), (lexer.has("par_der") ? {type: "par_der"} : par_der)], "postprocess":  data => {
          const tipo_dato = tabla.tipoModulo(data[0].toString())
          return { tipo: 'enunciado_llamada_modulo', nombre: data[0].toString(), argumentos: [], tipo_dato }
        } },
    {"name": "argumentos", "symbols": ["expresion"]},
    {"name": "argumentos", "symbols": ["expresion", (lexer.has("coma") ? {type: "coma"} : coma), "argumentos"], "postprocess": data => [ data[0], ...data[2] ]},
    {"name": "entero_literal", "symbols": [(lexer.has("entero") ? {type: "entero"} : entero)], "postprocess": data => Number(data[0])},
    {"name": "real_literal", "symbols": [(lexer.has("real") ? {type: "real"} : real)], "postprocess": data => Number(data[0])},
    {"name": "logico_literal", "symbols": [(lexer.has("logico") ? {type: "logico"} : logico)], "postprocess": data => data[0] != "falso"},
    {"name": "cadena_literal", "symbols": [(lexer.has("cadena") ? {type: "cadena"} : cadena)], "postprocess":  data => {
          const c = data[0].toString()
          return c.slice(1, c.length - 1)
        } },
    {"name": "il$ebnf$1", "symbols": [(lexer.has("espacio") ? {type: "espacio"} : espacio)], "postprocess": id},
    {"name": "il$ebnf$1", "symbols": [], "postprocess": () => null},
    {"name": "il", "symbols": ["il$ebnf$1"], "postprocess": nuller},
    {"name": "eb", "symbols": [(lexer.has("espacio") ? {type: "espacio"} : espacio)]},
    {"name": "eb", "symbols": [(lexer.has("nl") ? {type: "nl"} : nl)]},
    {"name": "fl$ebnf$1", "symbols": [(lexer.has("espacio") ? {type: "espacio"} : espacio)], "postprocess": id},
    {"name": "fl$ebnf$1", "symbols": [], "postprocess": () => null},
    {"name": "fl", "symbols": ["fl$ebnf$1", (lexer.has("nl") ? {type: "nl"} : nl)], "postprocess": nuller}
];

export var ParserStart: string = "sigma";
