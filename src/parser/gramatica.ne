@preprocessor typescript

@{%
const moo = require("moo");

import { TipoMatricial } from './tipado/TipoMatricial'
import { TipoReferencial } from './tipado/TipoReferencial'
import { TipoModulo } from './tipado/TipoModulo'
import { TipoReal } from './tipado/TipoReal'
import { TipoEntero } from './tipado/TipoEntero'
import { TipoLogico } from './tipado/TipoLogico'
import { TipoCaracter } from './tipado/TipoCaracter'
import { TipoCadena } from './tipado/TipoCadena'
import { TipoNinguno } from './tipado/TipoNinguno'
import { tiparLiteral } from './tipado/tiparLiteral'
import { OperacionAritmetica } from './tipado/operaciones/OperacionAritmetica'
import { OperacionLogica } from './tipado/operaciones/OperacionLogica'
import { OperacionRelacional } from './tipado/operaciones/OperacionRelacional'
import { OperacionComparativa } from './tipado/operaciones/OperacionComparativa'
import { OperacionDivisionReal } from './tipado/operaciones/OperacionDivisionReal'
import { OperacionDivisionEntera } from './tipado/operaciones/OperacionDivisionEntera'
import { OperacionNegativo } from './tipado/operaciones/OperacionNegativo'
import { OperacionNot } from './tipado/operaciones/OperacionNot'

import { TablaSimbolos } from './TablaSimbolos'

const lexer = moo.compile({
  cadena:             /"(?:\\["bfnrt\/\\]|\\u[a-fA-F0-9]{4}|[^"\\])*"/,
  sino:               /sino/,
  si:                 /si/,
  entonces:           /entonces/,
  fin_procedimiento:  /fin *procedimiento/,
  fin_mientras:       /fin *mientras/,
  fin_funcion:        /fin *funcion/,
  fin_para:           /fin *para/,
  fin_tipo:           /fin *tipo/,
  fin_si:             /fin *si/,
  fin:                /fin/,
  tipo:               /tipo/,
  inicio:             /inicio/,
  para:               /para/,
  variables:          /variables/,
  ref:                /ref/,
  repetir:            /repetir/,
  retornar:           /retornar/,
  hasta:              /hasta/,
  mientras:           /mientras/,
  separador:          / *: */,
  mas:                / *\+ */,
  accesor:            / *-> */,
  menos:              / *- */,
  not:                / *! */,
  elevar:             / *\*\* */,
  por:                / *\* */,
  barra:              / *\/ */,
  div:                / *div */,
  mod:                / +mod +/,
  and:                / *&& */,
  or:                 / *\|\| */,
  asignador:          / *<- */,
  menor_igual:        / *<= */,
  menor:              / *< */,
  mayor_igual:        / *>= */,
  mayor:              / *> */,
  igual:              / *== */,
  diferente:          / *!= */,
  real:               /[0-9]+\.[0-9]+/,
  entero:             /[0-9]+/,
  logico:             /falso|verdadero/,
  par_izq:            /\( */,
  par_der:            / *\)/,
  cor_izq:            /\[ */,
  cor_der:            / *\]/,
  identificador:      /[_a-zA-Z]+/,
  coma:               / *, */,
  espacio:            / +/,
  nl:                 { match: /\n+/, lineBreaks: true },
});

function flatten(arr: any[]): any[] {
  return arr.reduce((a, b) => [...a, ...flatten_if_arr(b)], [])
}

function flatten_if_arr(i: any): any[] {
  return i instanceof Array ? flatten(i):[i];
}

function nuller(): null {
  return null
}

export const tabla = new TablaSimbolos();
%}

@lexer lexer

sigma -> programa {% data => {
  tabla.finalizar()
  return data[0]
} %}

programa ->
    tad:* principal fl:? eb:* {% data => {
      return {
        tipo: 'programa',
        modulos: [ data[1] ],
        tads: data[0] == null ? []:data[0]
      }
    } %}
  | tad:* principal fl eb:* modulos {% data => {
      return {
        tipo: 'programa',
        modulos: [ data[1], ...data[4] ],
        tads: data[0] == null ? []:data[0]
      }
  } %}

tad -> tipo nombre_tipo miembros fin_tipo {% data => {
  return {
    tipo: 'tad',
    nombre: data[1],
    miembros: data[2].map((miembro: {}) => { return { ...miembro, tad_padre: data[1] } })
  }
} %}

tipo -> il:? %tipo fl eb:* {% nuller %}

nombre_tipo -> il:? %identificador fl eb:*  {% data => data[1].toString(0) %}

miembros -> declaraciones {% data => data[0].map((declaracion: {}) => {
  return { ...declaracion, tipo: 'declaracion_miembro' }
}) %}

fin_tipo -> il:? %fin_tipo fl eb:* {% nuller %}

principal -> variables declaraciones:? inicio enunciados:? fin {% data => {
  const declaraciones = data[1] == null ? []:flatten(data[1])
  const principal = {
    tipo: 'modulo',
    nombre: 'principal',
    enunciados: data[3] == null ? []:flatten(data[3]),
    declaraciones: data[1] == null ? []:flatten(data[1]),
    parametros: [],
    tipo_dato: new TipoModulo('principal', [], new TipoNinguno())
  }

  tabla.declararModulo(principal as any)

  return principal
} %}

modulos ->
    modulo fl:? eb:* {% data => [ data[0] ] %}
  | modulo fl modulos {% data => [ data[0], ...data[2] ] %}

modulo ->
    funcion {% id %}
  | procedimiento {% id %}

variables -> %espacio:? %variables %espacio:? %nl {% nuller %}

inicio -> %espacio:? %inicio %espacio:? %nl {% nuller %}

fin -> %espacio:? %fin %espacio:? {% nuller %}

funcion -> expresion_tipo nombre_modulo lista_parametros declaraciones:? inicio enunciados:? fin_funcion {% data => {
  const funcion = {
    tipo: 'modulo',
    nombre: data[1],
    enunciados: data[5] == null ? []:flatten(data[5]),
    declaraciones: data[3] == null ? []:flatten(data[3]),
    parametros: data[2],
    tipo_dato: new TipoModulo(data[1], data[2], data[0])
  }

  tabla.declararModulo(funcion as any)

  return funcion
} %}

procedimiento -> nombre_modulo lista_parametros declaraciones:? inicio enunciados:? fin_procedimiento {% data => {
  const procedimiento = {
    tipo: 'modulo',
    nombre: data[0],
    enunciados: data[4] == null ? []:flatten(data[4]),
    declaraciones: data[2] == null ? []:flatten(data[2]),
    parametros: data[1],
    tipo_dato: new TipoModulo(data[0], data[1], new TipoNinguno())
  }

  tabla.declararModulo(procedimiento as any)

  return procedimiento
} %}

nombre_modulo -> %espacio:? %identificador %espacio:?  {% data => data[1].toString(0) %}

lista_parametros -> %par_izq parametros %par_der {% data => data[1] %}

parametros ->
    %espacio:? declaracion {% data => [ data[1] ] %}
  | %espacio:? declaracion %coma parametros {% data => [ data[1], ...data[3] ] %}

fin_funcion -> %espacio:? %fin_funcion %espacio:? {% nuller %}

fin_procedimiento -> %espacio:? %fin_procedimiento %espacio:? {% nuller %}

declaraciones ->
    %espacio:? %nl {% data => [] %}
  | %espacio:? declaracion %nl {% data => {
      // tabla.declararVariable(data[1] as any)
      return [ data[1] ]
    } %}
  | %espacio:? declaracion %coma declaraciones {% data => {
      // const declaraciones = [ data[1], ...data[3] ]

      /*
      for (let declaracion of declaraciones) {
        tabla.declararVariable(declaracion as any)
      }
      */

      return [ data[1], ...data[3] ]
    } %}

declaracion -> %identificador %separador expresion_tipo {% data => {
  const declaracion = {
    tipo: 'declaracion',
    identificador: data[0].toString(),
    tipo_dato: data[2]
  }

  return declaracion
} %}

# expresion_tipo ->
#     tipo_atomico operadores_tipo {% data => {
#         let resultado = data[0]
#         for (let operador of (data[1] as any)) {
#           if (operador.tipo == 'ref') {
#             resultado = new TipoReferencial(resultado)
#           } else if (operador.tipo == 'dimension') {
#             resultado = new TipoMatricial(resultado as any, operador.dimensiones)
#           }
#         }
#         return resultado
#       } %}
#   | %identificador {% id %}

# operadores_tipo -> operador | operador operadores_tipo

# operador ->
#     %ref {% data => { return { tipo: 'ref' } } %}
#   | %cor_izq indices_enteros %cor_der {% data => { return { tipo: 'dimension', dimensiones: data[2] } } %}

# Expresion simplificada, solo para verficar hipotesis
# expresion_tipo -> tipo_atomico %cor_izq indices_enteros %cor_der {% data => new TipoMatricial(data[0], data[2]) %}

# Expresion de tipo con comportamiento indeseado (supuestamente)
expresion_tipo ->
    %ref %espacio:? tipo_matricial {% data => new TipoReferencial(data[2]) %}
  | tipo_matricial {% id %}

tipo_matricial ->
    tipo_entre_parentesis %cor_izq indices_enteros %cor_der {% data => new TipoMatricial(data[0], data[2]) %}
  | tipo_entre_parentesis {% id %}

tipo_entre_parentesis ->
    %par_izq expresion_tipo %par_der {% data => data[1] %}
  | tipo_atomico {% id %}

tipo_atomico -> %identificador {% data => {
  const nombre = data[0].toString()
  switch (nombre.toLowerCase()) {
    case 'entero':
      return new TipoEntero()
    case 'real':
      return new TipoReal();
    case 'logico':
      return new TipoLogico();
    case 'caracter':
      return new TipoCaracter();
    default:
      // Si se llega a este caso, se trada del nombre de un TAD
      return new TipoNinguno();
  }
} %}

indices_enteros ->
    entero_literal {% data => [ data[0] ] %}
  | entero_literal %coma indices_enteros {% data => [ data[0], ...data[2] ] %}

enunciados ->
    %espacio:? enunciado %nl {% data => [ data[1] ] %}
  | %espacio:? enunciado %nl enunciados {% data => [ data[1], ...data[3] ] %}

enunciado ->
    asignacion {% id %}
  | enunciado_llamada_modulo {% id %}
  | repetir {% id %}
  | mientras {% id %}
  | para {% id %}
  | si {% id %}
  | retornar {% id %}

repetir -> %repetir %espacio:? %nl enunciados %espacio:? %hasta %espacio:? expresion {% data => { 
  return {
    tipo: 'repetir',
    enunciados: data[3],
    condicion: data[7]
  }
} %}

mientras -> %mientras %espacio:? expresion %espacio:? %nl enunciados %espacio:? %fin_mientras {% data => {
  return {
    tipo: 'mientras',
    enunciados: data[5],
    condicion: data[2]
  }
} %}

para -> %para inicializador hasta valor_final enunciados:? fin_para {% data => { 
  let enunciados = []

  if (data[4]) {
    enunciados = data[4]
  }

  return {
    tipo: 'para',
    inicializador: data[1],
    valor_final: data[3],
    enunciados
  }
} %}

inicializador -> %espacio:? asignacion %espacio:? {% data => data[1] %}

valor_final -> expresion %espacio:? %nl {% id %}

fin_para -> %espacio:? %fin_para {% nuller %}

hasta -> %hasta %espacio:? {% nuller %}

si ->
    %si condicion entonces enunciados:? sino enunciados:? fin_si {% data => {
      let rama_verdadera = []
      let rama_falsa = []

      if (data[3]) {
        rama_verdadera = data[3]
      }

      if (data[5]) {
        rama_falsa = data[5]
      }

      return {
        tipo: 'si',
        rama_verdadera,
        rama_falsa,
        condicion: data[1]
      }
  } %}
  | %si condicion entonces enunciados:? fin_si {% data => {
      let rama_verdadera = []

      if (data[3]) {
        rama_verdadera = data[3]
      }

      return {
        tipo: 'si',
        rama_verdadera,
        rama_falsa:[],
        condicion: data[1]
      }
    } %}

condicion -> %espacio:? expresion %espacio:? {% data => data[1] %}

entonces -> %entonces %espacio:? %nl {% nuller %}

sino -> %espacio:? %sino %espacio:? %nl {% nuller %}

fin_si -> %espacio:? %fin_si %espacio:? {% nuller %}

retornar -> %retornar %espacio:? expresion {% data => { return { tipo: 'retornar', valor: data[2] } } %}

asignacion ->
    invocacion %asignador expresion {% data => {
      return { tipo: 'asignacion', variable: data[0], expresion: data[2] }
    } %}

expresion -> relacional {% id %}

relacional -> 
    relacional %and aditiva {% data => {
      return {
        tipo: 'operacion',
        subtipo: 'and',
        aridad: 'binaria',
        a: data[0],
        b: data[2],
        tipo_calculado: new OperacionLogica(data[0], data[2])
      }
    } %}
  | relacional %or aditiva {% data => {
      return {
        tipo: 'operacion',
        subtipo: 'or',
        aridad: 'binaria',
        a: data[0],
        b: data[2],
        tipo_calculado: new OperacionLogica(data[0], data[2])
      }
  } %}
  | relacional %menor aditiva {% data => {
      return {
        tipo: 'operacion',
        subtipo: 'menor',
        aridad: 'binaria',
        a: data[0],
        b: data[2],
        tipo_calculado: new OperacionRelacional(data[0], data[2])
      }
  } %}
  | relacional %menor_igual aditiva {% data => {
      return {
        tipo: 'operacion',
        subtipo: 'menor_igual',
        aridad: 'binaria',
        a: data[0],
        b: data[2],
        tipo_calculado: new OperacionRelacional(data[0], data[2])
      }
  } %}
  | relacional %mayor aditiva {% data => {
      return {
        tipo: 'operacion',
        subtipo: 'mayor',
        aridad: 'binaria',
        a: data[0],
        b: data[2],
        tipo_calculado: new OperacionRelacional(data[0], data[2])
      }
  } %}
  | relacional %mayor_igual aditiva {% data => {
      return {
        tipo: 'operacion',
        subtipo: 'mayor_igual',
        aridad: 'binaria',
        a: data[0],
        b: data[2],
        tipo_calculado: new OperacionRelacional(data[0], data[2])
      }
  } %}
  | relacional %igual aditiva {% data => {
      return {
        tipo: 'operacion',
        subtipo: 'igual',
        aridad: 'binaria',
        a: data[0],
        b: data[2],
        tipo_calculado: new OperacionComparativa(data[0], data[2])
      }
  } %}
  | relacional %diferente aditiva {% data => {
      return {
        tipo: 'operacion',
        subtipo: 'diferente',
        aridad: 'binaria',
        a: data[0],
        b: data[2],
        tipo_calculado: new OperacionComparativa(data[0], data[2])
      }
  } %}
  | aditiva {% id %}

aditiva ->
    aditiva %mas multiplicativa {% data => {
      return {
        tipo: 'operacion',
        subtipo: 'mas',
        aridad: 'binaria',
        a: data[0],
        b: data[2],
        tipo_calculado: new OperacionAritmetica(data[0], data[2])
      }
    } %}
  | aditiva %menos multiplicativa {% data => {
      return {
        tipo: 'operacion',
        subtipo: 'menos',
        aridad: 'binaria',
        a: data[0],
        b: data[2],
        tipo_calculado: new OperacionAritmetica(data[0], data[2])
      }
  } %}
  | multiplicativa {% id %}

multiplicativa ->
    multiplicativa %por   entre_parentensis {% data => {
      return {
        tipo: 'operacion',
        subtipo: 'por',
        aridad: 'binaria',
        a: data[0],
        b: data[2],
        tipo_calculado: new OperacionAritmetica(data[0], data[2])
      }
    } %}
  | multiplicativa %barra entre_parentensis {% data => {
    return {
      tipo: 'operacion',
      subtipo: 'barra',
      aridad: 'binaria',
      a: data[0],
      b: data[2],
      tipo_calculado: new OperacionDivisionReal(data[0], data[2])
    }
  } %}
  | multiplicativa %div   entre_parentensis {% data => {
    return {
      tipo: 'operacion',
      subtipo: 'div',
      aridad: 'binaria',
      a: data[0],
      b: data[2],
      tipo_calculado: new OperacionDivisionEntera(data[0], data[2])
    }
  } %}
  | multiplicativa %mod   entre_parentensis {% data => {
    return {
      tipo: 'operacion',
      subtipo: 'mod',
      aridad: 'binaria',
      a: data[0],
      b: data[2],
      tipo_calculado: new OperacionDivisionEntera(data[0], data[2])
    }
  } %}
  | exponencial {% id %}

exponencial ->
    exponencial %elevar unaria {% data => {
      return {
        tipo: 'operacion',
        subtipo: 'elevar',
        aridad: 'binaria',
        a: data[0],
      b: data[2],
        tipo_calculado: new OperacionAritmetica(data[0], data[2])
      }
    } %}
  | unaria {% id %}

unaria ->
    %menos entre_parentensis {% data => {
      return {
        tipo: 'operacion',
        subtipo: 'negativo',
        aridad: 'unaria',
        a: data[1],
        tipo_calculado: new OperacionNegativo(data[1])
      }
    } %}
  | %not entre_parentensis {% data => {
      return {
        tipo: 'operacion',
        subtipo: 'not',
        aridad: 'unaria',
        a: data[1],
        tipo_calculado: new OperacionNot(data[1])
      }
    } %}
  | entre_parentensis {% id %}

entre_parentensis ->
    %par_izq expresion %par_der {% data => data[1] %}
  | atomica {% id %}

atomica ->
    literal {% id %}
  | invocacion {% id %}
  | llamada_modulo {% id %}

literal ->
    entero_literal {% data => { 
      return { tipo: 'operando', subtipo: 'literal', valor: data[0], tipo_dato: tiparLiteral(data[0]) } }
    %}
  | real_literal {% data => { 
    return { tipo: 'operando', subtipo: 'literal', valor: data[0], tipo_dato: tiparLiteral(data[0]) } }
  %}
  | cadena_literal {% data => { 
    return { tipo: 'operando', subtipo: 'literal', valor: data[0], tipo_dato: tiparLiteral(data[0]) } }
  %}
  | logico_literal {% data => { 
    return { tipo: 'operando', subtipo: 'literal', valor: data[0], tipo_dato: tiparLiteral(data[0]) } }
  %}

invocacion ->
    invocacion_miembro {% id %}
  | invocacion_variable {% id %}

invocacion_miembro ->
    invocacion_variable %accesor invocacion_variable {% data => {
      return { tipo: 'miembro', padre: data[0], miembro: data[2] }
    } %}
  | invocacion_variable %accesor invocacion_miembro {% data => {
      return { tipo: 'miembro', padre: data[0], miembro: data[2] }
    } %}

invocacion_variable ->
    %identificador %cor_izq indices %cor_der {% data => {
      const invocacion = { tipo: 'operando', subtipo: 'variable', nombre: data[0].toString(), indices: data[2], tipo_dato: null }
      tabla.tiparInvocacion(invocacion as any)
      return invocacion
    } %}
  | %identificador {% data => {
      const invocacion = { tipo: 'operando', subtipo: 'variable', nombre: data[0].toString(), indices: [], tipo_dato: null }
      tabla.tiparInvocacion(invocacion as any)
      return invocacion
    } %}

indices -> 
    expresion 
  | expresion %coma indices {% data => [ data[0], ...data[2] ] %}

llamada_modulo ->
    %identificador %par_izq argumentos %par_der {% data => {
      const tipo_dato = tabla.tipoModulo(data[0].toString())
      return { tipo: 'operando', subtipo: 'llamada_modulo', nombre: data[0].toString(), argumentos: data[2], tipo_dato }
    } %}
  | %identificador %par_izq %par_der {% data => {
      const tipo_dato = tabla.tipoModulo(data[0].toString())
      return { tipo: 'operando', subtipo: 'llamada_modulo', nombre: data[0].toString(), argumentos: [], tipo_dato }
    } %}

enunciado_llamada_modulo ->
    %identificador %par_izq argumentos %par_der {% data => {
      const tipo_dato = tabla.tipoModulo(data[0].toString())
      return { tipo: 'enunciado_llamada_modulo', nombre: data[0].toString(), argumentos: data[2], tipo_dato }
    } %}
  | %identificador %par_izq %par_der {% data => {
      const tipo_dato = tabla.tipoModulo(data[0].toString())
      return { tipo: 'enunciado_llamada_modulo', nombre: data[0].toString(), argumentos: [], tipo_dato }
    } %}

argumentos ->
    expresion
  | expresion %coma argumentos {% data => [ data[0], ...data[2] ] %}

entero_literal -> %entero {% data => Number(data[0]) %}

real_literal -> %real {% data => Number(data[0]) %}

logico_literal -> %logico {% data => data[0] != "falso" %}

cadena_literal -> %cadena {% data => {
  const c = data[0].toString()
  return c.slice(1, c.length - 1)
} %}

# il: "inicio de linea"
il -> %espacio:? {% nuller %}

# eb: "espacio en blanco"
eb -> %espacio | %nl

# fl: "fin de linea"
fl -> %espacio:? %nl {% nuller %}