import { Expresion, Operacion } from "./TiposParser";
import { TipoDato } from './tipado/TipoDato';

export type ExpresionRPN = (Operador | Operando)[];

export type Operador =
  | And
  | Or
  | Menor
  | MenorIgual
  | Mayor
  | MayorIgual
  | Igual
  | Diferente
  | Mas
  | Menos
  | Por
  | Barra
  | Div
  | Mod
  | Elevar
  | Negativo
  | Not;

export type Operando = Llamada | Invocacion | Literal;

export type And = {
  tipo: "operador";
  nombre: "and";
};

export type Or = {
  tipo: "operador";
  nombre: "or";
};

export type Menor = {
  tipo: "operador";
  nombre: "menor";
};

export type MenorIgual = {
  tipo: "operador";
  nombre: "menor_igual";
};

export type Mayor = {
  tipo: "operador";
  nombre: "mayor";
};

export type MayorIgual = {
  tipo: "operador";
  nombre: "mayor_igual";
};

export type Igual = {
  tipo: "operador";
  nombre: "igual";
};

export type Diferente = {
  tipo: "operador";
  nombre: "diferente";
};

export type Mas = {
  tipo: "operador";
  nombre: "mas";
};

export type Menos = {
  tipo: "operador";
  nombre: "menos";
};

export type Por = {
  tipo: "operador";
  nombre: "por";
};

export type Barra = {
  tipo: "operador";
  nombre: "barra";
};

export type Div = {
  tipo: "operador";
  nombre: "div";
};

export type Mod = {
  tipo: "operador";
  nombre: "mod";
};

export type Elevar = {
  tipo: "operador";
  nombre: "elevar";
};

export type Negativo = {
  tipo: "operador";
  nombre: "negativo";
};

export type Not = {
  tipo: "operador";
  nombre: "not";
};

export type Llamada = {
  tipo: "operando";
  nombre: "llamada";
  nombreModulo: string;
  cantidadArgumentos: number;
  tipo_dato: TipoDato
};

export type Invocacion = {
  tipo: "operando";
  nombre: "invocacion";
  nombreVariable: string;
  cantidadIndices: number;
  tipo_dato: TipoDato
};

export type Literal = {
  tipo: "operando";
  nombre: "literal";
  valor: number | boolean | string;
};

export function arbol_a_rpn(expresion: Expresion): ExpresionRPN {
  // Si el tope es un operando, devolverlo
  // Si el tope es una operacion unaria, devolver su rama izquierda y el operador
  // Si el tope es una operacion binaria, devolver su rama izquierda, su rama derecha, y el operador
  switch (expresion.tipo) {
    case "operando":
      switch (expresion.subtipo) {
        case "literal":
          return [
            { tipo: "operando", nombre: "literal", valor: expresion.valor }
          ];
        case "llamada_modulo":
          return [
            ...expresiones_a_rpn(expresion.argumentos),
            {
              tipo: "operando",
              nombre: "llamada",
              nombreModulo: expresion.nombre,
              cantidadArgumentos: expresion.argumentos.length,
              tipo_dato: expresion.tipo_dato
            }
          ];
        case "variable":
          return [
            ...expresiones_a_rpn(expresion.indices),
            {
              tipo: "operando",
              nombre: "invocacion",
              nombreVariable: expresion.nombre,
              cantidadIndices: expresion.indices.length,
              tipo_dato: expresion.tipo_dato
            }
          ];
      }
      break;
    case "operacion":
      switch (expresion.aridad) {
        case "unaria":
          return [...arbol_a_rpn(expresion.a), operacion_a_operador(expresion)];
        case "binaria":
          return [
            ...arbol_a_rpn(expresion.a),
            ...arbol_a_rpn(expresion.b),
            operacion_a_operador(expresion)
          ];
      }
  }
}

function expresiones_a_rpn(expresiones: Expresion[]): ExpresionRPN {
  let resultado: ExpresionRPN = [];

  for (let e of expresiones) {
    resultado = [...resultado, ...arbol_a_rpn(e)];
  }

  return resultado;
}

function operacion_a_operador(operacion: Operacion): Operador {
  return { tipo: "operador", nombre: operacion.subtipo as any };
}
