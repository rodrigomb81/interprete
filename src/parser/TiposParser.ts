import { TipoCalculado } from './tipado/operaciones/TipoCalculado'
import { TipoDato } from './tipado/TipoDato'
import { TipoModulo } from './tipado/TipoModulo'
import { TipoReal } from './tipado/TipoReal'
import { TipoEntero } from './tipado/TipoEntero'
import { TipoLogico } from './tipado/TipoLogico'
import { TipoCaracter } from './tipado/TipoCaracter'
import { TipoCadena } from './tipado/TipoCadena'

/**
 * Tipo de datos para el nodo raiz del
 * Arbol Abstracto de Sintaxis (AST, en ingles).
 */
export type AST = {
  tipo: 'programa'
  modulos: Modulo[]
  tads: TipoAbstracto[]
}

export type Modulo = {
  tipo: 'modulo'
  nombre: string
  enunciados: Enunciado[],
  declaraciones: Declaracion[]
  tipo_dato: TipoModulo
  parametros: Declaracion[]
}

export type TipoAbstracto = {
  tipo: 'tad'
  nombre: string
  miembros: Miembro[]
}

export type Miembro = {
  tipo: 'declaracion_miembro'
  identificador: string
  tipo_dato: TipoDato
  tad_padre: 'string'
}

export type Enunciado =
    Asignacion
  | EnunciadoLlamadaModulo
  | Repetir
  | Mientras
  | Para
  | Si
  | Retornar

export type Asignacion = {
  tipo: 'asignacion'
  variable: InvocacionVariable
  expresion: Expresion
}

export type Repetir = {
  tipo: 'repetir'
  enunciados: Enunciado[]
  condicion: Expresion
}

export type Mientras = {
  tipo: 'mientras'
  enunciados: Enunciado[]
  condicion: Expresion
}

export type Para = {
  tipo: 'para'
  inicializador: Asignacion
  valor_final: Expresion
  enunciados: Enunciado[]
}

export type Si = {
  tipo: 'si'
  condicion: Expresion
  rama_verdadera: Enunciado[]
  rama_falsa: Enunciado[]
}

export type Retornar = {
  tipo: 'retornar'
  valor: Expresion
}

export type Declaracion = {
  tipo: 'declaracion'
  identificador: string
  tipo_dato: TipoDato
}

export type Expresion =
    Relacional
  | Aditiva
  | Multiplicativa
  | Exponencial
  | Unaria
  | Atomica

export type Operacion = 
    Relacional
  | Aditiva
  | Multiplicativa
  | Exponencial
  | Unaria

export type Relacional =
    And
  | Or
  | Menor
  | MenorIgual
  | Mayor
  | MayorIgual
  | Igual
  | Diferente

export type And = {
  tipo: 'operacion'
  subtipo: 'and'
  aridad: 'binaria'
  tipo_calculado: TipoCalculado
  a: Expresion
  b: Expresion
}

export type Or = {
  tipo: 'operacion'
  subtipo: 'or'
  aridad: 'binaria'
  tipo_calculado: TipoCalculado
  a: Expresion
  b: Expresion
}

export type Menor = {
  tipo: 'operacion'
  subtipo: 'menor'
  aridad: 'binaria'
  tipo_calculado: TipoCalculado
  a: Expresion
  b: Expresion
}

export type MenorIgual = {
  tipo: 'operacion'
  subtipo: 'menor_igual'
  aridad: 'binaria'
  tipo_calculado: TipoCalculado
  a: Expresion
  b: Expresion
}

export type Mayor = {
  tipo: 'operacion'
  subtipo: 'mayor'
  aridad: 'binaria'
  tipo_calculado: TipoCalculado
  a: Expresion
  b: Expresion
}

export type MayorIgual = {
  tipo: 'operacion'
  subtipo: 'mayor_igual'
  aridad: 'binaria'
  tipo_calculado: TipoCalculado
  a: Expresion
  b: Expresion
}

export type Igual = {
  tipo: 'operacion'
  subtipo: 'igual'
  aridad: 'binaria'
  tipo_calculado: TipoCalculado
  a: Expresion
  b: Expresion
}

export type Diferente = {
  tipo: 'operacion'
  subtipo: 'diferente'
  aridad: 'binaria'
  tipo_calculado: TipoCalculado
  a: Expresion
  b: Expresion
}

export type Aditiva = Suma | Resta

export type Suma = {
  tipo: 'operacion'
  subtipo: 'mas'
  aridad: 'binaria'
  tipo_calculado: TipoCalculado
  a: Expresion
  b: Expresion
}

export type Resta = {
  tipo: 'operacion'
  subtipo: 'menos'
  aridad: 'binaria'
  tipo_calculado: TipoCalculado
  a: Expresion
  b: Expresion
}

export type Multiplicativa =
    Multiplicacion
  | DivisionReal
  | DivisionEntera
  | OperadorModulo

export type Multiplicacion = {
  tipo: 'operacion'
  subtipo: 'por'
  aridad: 'binaria'
  tipo_calculado: TipoCalculado
  a: Expresion
  b: Expresion
}

export type DivisionReal = {
  tipo: 'operacion'
  subtipo: 'barra'
  aridad: 'binaria'
  tipo_calculado: TipoCalculado
  a: Expresion
  b: Expresion
}

export type DivisionEntera = {
  tipo: 'operacion'
  subtipo: 'div'
  aridad: 'binaria'
  tipo_calculado: TipoCalculado
  a: Expresion
  b: Expresion
}

export type OperadorModulo = {
  tipo: 'operacion'
  subtipo: 'mod'
  aridad: 'binaria'
  tipo_calculado: TipoCalculado
  a: Expresion
  b: Expresion
}

export type Exponencial = {
  tipo: 'operacion'
  subtipo: 'elevar'
  aridad: 'binaria'
  tipo_calculado: TipoCalculado
  a: Expresion
  b: Expresion
}

export type Unaria = Negativo | Not

export type Negativo = {
  tipo: 'operacion'
  subtipo: 'negativo'
  aridad: 'unaria'
  tipo_calculado: TipoCalculado
  a: Expresion
}

export type Not = {
  tipo: 'operacion'
  subtipo: 'not'
  aridad: 'unaria'
  tipo_calculado: TipoCalculado
  a: Expresion
}

export type Atomica =
    Literal
  | LlamadaModulo
  | Invocacion

export type Literal = {
  tipo: 'operando'
  subtipo: 'literal'
  valor: number | boolean | string
  tipo_dato: TipoEntero | TipoReal | TipoLogico | TipoCadena | TipoCaracter
}

export type LlamadaModulo = {
  tipo: 'operando'
  subtipo: 'llamada_modulo',
  nombre: string,
  argumentos: Expresion[]
  tipo_dato: TipoModulo
}

export type EnunciadoLlamadaModulo = {
  tipo: 'enunciado_llamada_modulo'
  nombre: string,
  argumentos: Expresion[]
  tipo_dato: TipoModulo
}

// La siguiente linea queda comentada hasta que el uso de TADS
// sea soportado por completo.
// export type Invocacion = InvocacionVariable | InvocacionMiembro
export type Invocacion = InvocacionVariable

export type InvocacionVariable = {
  tipo: 'operando'
  subtipo: 'variable'
  nombre: string
  indices: Expresion[]
  tipo_dato: TipoDato
}

export type InvocacionMiembro = {
  tipo: 'miembro'
  padre: InvocacionVariable
  miembro: Invocacion
}