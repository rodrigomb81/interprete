export type Opcional<A> = Existente<A> | Inexistente

export type Existente<A> = {
  readonly existe: true
  valor: A
}

export type Inexistente = {
  readonly existe: false
  valor: null
}