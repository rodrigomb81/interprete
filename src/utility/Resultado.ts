export type Resultado<A, B> = Exito<A> | Fallo<B>

type Exito<A> = {
  error: false,
  valor: A
}

type Fallo<B> = {
  error: true,
  valor: B
}