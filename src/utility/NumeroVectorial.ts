export class NumeroVectorial {

  private dimensiones: number[]

  private valorActual: number[]

  constructor(dimensiones: number[]) {
    this.dimensiones = dimensiones

    this.valorActual = new Array(this.dimensiones.length)
    for (let i = 0; i < this.dimensiones.length; i++) {
      this.valorActual[i] = 1
    }
  }

  setValor(valor: number[]) {
    for (let i = 0; i < this.dimensiones.length && i < valor.length; i++) {
      this.valorActual[i] = valor[i]
    }
  }

  /**
   * Aumenta este vector en uno.
   */
  aumentar() {
    let i = this.dimensiones.length - 1
    let incrementoPendiente = true
    while (i >= 0 && incrementoPendiente) {
      this.valorActual[i]++
      if (i > 0 && this.valorActual[i] > this.dimensiones[i]) {
        this.valorActual[i] = 1
        i--
      } else {
        incrementoPendiente = false
      }
    }
  }

  /**
   * Decrementa este vector en uno.
   */
  decrementar() {
    let i = this.dimensiones.length - 1
    let decrementoPendiente = true
    while (i >= 0 && decrementoPendiente) {
      this.valorActual[i]--
      if (i > 0 && this.valorActual[i] < 1) {
        this.valorActual[i] = this.dimensiones[i]
        i--
      } else {
        decrementoPendiente = false
      }
    }
  }

  comoArreglo(): number[] {
    return this.valorActual
  }

  /**
   * Dice si este vector es igual a otro.
   * @param vector vector a comparar
   */
  igual(vector: number[]): boolean {
    if (vector.length < this.dimensiones.length) {
      return false
    } else {
      let i = 0
      let iguales = true
      while (i < this.dimensiones.length && iguales) {
        iguales = vector[i] == this.valorActual[i]
        i++
      }
      return iguales
    }
  }

  /**
   * Dice si este vector es menor que otro.
   * 
   * @param vector vector a comparar
   */
  menor(vector: number[]): boolean {
    let i = 0
    let menor = false
    while (i < this.dimensiones.length && i < vector.length && !menor && this.valorActual[i] <= vector[i]) {
      menor = this.valorActual[i] < vector[i]
      i++
    }
    return menor
  }
}