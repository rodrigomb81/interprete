export class Rango implements Iterable<number> {
  readonly inicio: number
  readonly fin: number
  readonly paso: number

  constructor(inicio: number, fin: number, paso?: number) {
    this.inicio = inicio;
    this.fin = fin;
    if (paso) {
      this.paso = paso;
    } else {
      this.paso = 1;
    }
  }

  [Symbol.iterator](): Iterator<number> {
    return new IteradorRango(this)
  }
}

class IteradorRango implements Iterator<number> {
  private rango: Rango
  private valor: number

  constructor(r: Rango) {
    this.rango = r
    this.valor = r.inicio
  }

  next(): IteratorResult<number> {
    const done = this.valor == this.rango.fin + 1
    if (done) {
      return { done, value: null }
    } else {
      const value = this.valor
      this.valor++
      return { done, value }
    }
  }
}