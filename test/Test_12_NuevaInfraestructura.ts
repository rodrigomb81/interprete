import "should";

import { Compilador } from "../src/NuevoCompilador";

import { MaquinaVirtual } from "../src/runtime_env/MaquinaVirtual";

import { Parser as NewParser } from "../src/parser/NewParser";

const compilador = new Compilador(false);

describe("Nueva infraestructura", () => {
  describe("Programas de un solo modulo", () => {
    it("Programa nulo", () => {
      const code = `variables
      inicio
      fin`;

      const modulos = compilador.compilar(code);

      const evaluador = new MaquinaVirtual();

      evaluador.setPrograma(modulos);

      evaluador.ejecutar();
    });

    it("Programa con una asignacion a una variable", () => {
      const code = `variables
      a: entero
      inicio
      a <- 2
      fin`;

      const modulos = compilador.compilar(code);

      const evaluador = new MaquinaVirtual();

      evaluador.setPrograma(modulos);

      evaluador.ejecutar();

      const escalar = evaluador.recuperarEscalarGlobal("a");

      escalar.valor().should.equal(2);
    });

    it("Programa con una asignacion a una variable vectorial", () => {
      const code = `variables
      a: entero[2]
      inicio
      a[1] <- 1
      a[2] <- 2
      fin`;

      const modulos = compilador.compilar(code);

      const evaluador = new MaquinaVirtual();

      evaluador.setPrograma(modulos);

      evaluador.ejecutar();

      const vector = evaluador.recuperarVectorGlobal("a");

      vector
        .obtenerCelda([1])
        .valor()
        .should.equal(1);

      vector
        .obtenerCelda([2])
        .valor()
        .should.equal(2);
    });

    it("Programa que copia un vector a otro", () => {
      const code = `variables
        a: entero[2], b: entero[2]
        inicio
        a[1] <- 1
        a[2] <- 2
        b <- a
        fin`;

      const modulos = compilador.compilar(code);

      const evaluador = new MaquinaVirtual();

      evaluador.setPrograma(modulos);

      evaluador.ejecutar();

      const vectorB = evaluador.recuperarVectorGlobal("b");

      const b1 = vectorB.obtenerCelda([1]).valor() == 1;

      b1.should.equal(true);

      const b2 = vectorB.obtenerCelda([2]).valor() == 2;

      b2.should.equal(true);
    });

    it("Programa que copia una matriz a otra", () => {
      const code = `variables
      a: entero[2, 2], b: entero[2, 2]
      inicio
      a[1, 1] <- 1
      a[1, 2] <- 2
      a[2, 1] <- 3
      a[2, 2] <- 4
      b <- a
      fin`;

      const modulos = compilador.compilar(code);

      const evaluador = new MaquinaVirtual();

      evaluador.setPrograma(modulos);

      evaluador.ejecutar();

      const b = evaluador.recuperarVectorGlobal("b");

      const b11 = b.obtenerCelda([1, 1]).valor() == 1;

      b11.should.equal(true);

      const b12 = b.obtenerCelda([1, 2]).valor() == 2;

      b12.should.equal(true);

      const b13 = b.obtenerCelda([2, 1]).valor() == 3;

      b13.should.equal(true);

      const b14 = b.obtenerCelda([2, 2]).valor() == 4;

      b14.should.equal(true);
    });

    it("Programa con una estructura de seleccion que ejecuta la rama verdadera", () => {
      const code = `variables
          a: entero
      inicio
      a <- 2
      si (a == 2) entonces
      a <- 5000
      sino
      a <- 6000
      fin si
      fin`;
      
      const modulos = compilador.compilar(code);

      const evaluador = new MaquinaVirtual();

      evaluador.setPrograma(modulos);

      evaluador.ejecutar();

      const asignacion = evaluador.recuperarEscalarGlobal('a').valor() == 5000;

      asignacion.should.equal(true);
    });

    it('Programa con una estructura de seleccion que ejecuta la rama falsa', () => {
      const code = `variables
      a: entero
      inicio
      a <- 1
      si (a == 2) entonces
      a <- 5000
      sino
      a <- 6000
      finsi
      fin`
      const modulos = compilador.compilar(code);

      const evaluador = new MaquinaVirtual();

      evaluador.setPrograma(modulos);

      evaluador.ejecutar();

      const asignacion = evaluador.recuperarEscalarGlobal('a').valor() == 6000;

      asignacion.should.equal(true);
    })

    it('Programa que incrementa un contador con una estructura mientras', () => {
        const code = `variables
        a: entero
        inicio
        a <- 0
        mientras (a < 3)
        a <- a + 1
        fin mientras
        fin`
        const modulos = compilador.compilar(code);

        const evaluador = new MaquinaVirtual();
  
        evaluador.setPrograma(modulos);
  
        evaluador.ejecutar();
  
        const asignacion = evaluador.recuperarEscalarGlobal('a').valor() == 3;
  
        asignacion.should.equal(true);
    })

    it('Programa que saltea un bucle mientras', () => {
      const code = `variables
      a: entero
      inicio
      a <- 0
      mientras (a < 0)
      a <- a + 1
      fin mientras
      fin`
      const modulos = compilador.compilar(code);

      const evaluador = new MaquinaVirtual();

      evaluador.setPrograma(modulos);

      evaluador.ejecutar();

      const asignacion = evaluador.recuperarEscalarGlobal('a').valor() == 0;

      asignacion.should.equal(true);
  })

    it('Programa que incrementa un contador con una estructura repetir', () => {
      const code = `variables
      a: entero
      inicio
      a <- 0
      repetir
        a <- a + 1
      hasta (a == 3)
      fin`
      const modulos = compilador.compilar(code);

      const evaluador = new MaquinaVirtual();

      evaluador.setPrograma(modulos);

      evaluador.ejecutar();

      const asignacion = evaluador.recuperarEscalarGlobal('a').valor() == 3;

      asignacion.should.equal(true);
    })

    it('Programa que solo ejecuta una vez un bucle repetir', () => {
      const code = `variables
      a: entero
      inicio
      a <- 0
      repetir
        a <- a + 1
      hasta (a == 1)
      fin`
      const modulos = compilador.compilar(code);

      const evaluador = new MaquinaVirtual();

      evaluador.setPrograma(modulos);

      evaluador.ejecutar();

      const asignacion = evaluador.recuperarEscalarGlobal('a').valor() == 1;

      asignacion.should.equal(true);
    })

    it('Programa que incrementa un contador con una estructura para', () => {
      const code = `variables
      a: entero, b: entero
      inicio
      para a <- 0 hasta 3
          b <- 2000
      fin para
      fin`
      const modulos = compilador.compilar(code);

      const evaluador = new MaquinaVirtual();

      evaluador.setPrograma(modulos);

      evaluador.ejecutar();

      const asignacionA = evaluador.recuperarEscalarGlobal('a').valor() == 4

      asignacionA.should.equal(true)

      const asignacionB = evaluador.recuperarEscalarGlobal('b').valor() == 2000

      asignacionB.should.equal(true)
    })

    it('Bucle PARA con un contador en una celda de un vector ', () => {
        const code = `variables
            a: entero[2]
        inicio
        para a[1] <- 0 hasta 3
        fin para
        fin`
        const modulos = compilador.compilar(code);

        const evaluador = new MaquinaVirtual();
  
        evaluador.setPrograma(modulos);
  
        evaluador.ejecutar();

        const a1 = evaluador.recuperarVectorGlobal('a').obtenerCelda([1]).valor() == 4

        a1.should.equal(true)
    })

    it('Si el contador de un bucle PARA es mayor al valor final, el bucle es salteado', () => {
        const code = `variables
        a: entero, b: entero
        inicio
        b <- 3000
        para a <- 4 hasta 3
            b <- 2000
        finpara
        fin`

        const modulos = compilador.compilar(code);

        const evaluador = new MaquinaVirtual();
  
        evaluador.setPrograma(modulos);
  
        evaluador.ejecutar();

        const asignacion = evaluador.recuperarEscalarGlobal('b').valor() == 3000

        asignacion.should.equal(true)
    })

    it('Programa con un llamado a ESCRIBIR con un solo argumento', () => {
      const code = `variables
      a: entero
      inicio
      escribir(38)
      a <- 1000
      fin`

      const modulos = compilador.compilar(code);

      const evaluador = new MaquinaVirtual();

      evaluador.setPrograma(modulos);

      evaluador.ejecutar();

      // probar que haya una escritura pendiente
      let hayEscrituraPendiente = evaluador.hayEscrituraPendiente()

      hayEscrituraPendiente.should.equal(true)
      
      const valorEscrito = evaluador.getProximaEscritura()

      valorEscrito.should.equal(38)

      const finAlcanzado = evaluador.finAlcanzado()

      finAlcanzado.should.equal(false)

      evaluador.resumir()

      const asignacion = evaluador.recuperarEscalarGlobal('a').valor() == 1000

      asignacion.should.equal(true)
    })

    it('Programa con un llamado a LEER con un solo argumento', () => {
      const code = `variables
      a: entero
      inicio
      leer(a)
      fin`

      const modulos = compilador.compilar(code);

      const evaluador = new MaquinaVirtual();

      evaluador.setPrograma(modulos);

      evaluador.ejecutar();

      let hayLecturaPendiente = evaluador.hayLecturaPendiente()

      hayLecturaPendiente.should.equal(true)

      evaluador.enviarEntrada(32)

      const finAlcanzado = evaluador.finAlcanzado()

      finAlcanzado.should.equal(false)

      evaluador.resumir()

      const asignacion = evaluador.recuperarEscalarGlobal('a').valor() == 32

      asignacion.should.equal(true)
    })
  });
});
